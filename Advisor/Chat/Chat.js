// Advisor/Chat/Chat.js
Page({

  /**
   * 页面的初始数据
   */
  data: {

  },
  navTabbar(e){
    console.log(e.currentTarget.dataset.url)
    wx.reLaunch({
      url: `../${e.currentTarget.dataset.url}/${e.currentTarget.dataset.url}`,
    })
  },

  onShow(){
    wx.hideHomeButton({
      success: (res) => {},
    })
  }

})