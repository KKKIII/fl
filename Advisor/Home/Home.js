// Advisor/Home/Home.js
const api = require('../../config/api.js')
const app = getApp()


Page({

  /**
   * 页面的初始数据
   */
  data: {
    show: false,
    showconfirmbutton: false,
    box: 1,
    email: app.globalData.email,
    showLength:5,
    course: [{
      id: '0',
      course_image: '../../images/tabbarLogo/2410615 1.png',
      course_name: 'Java'
    },{
      id: '1',
      course_image: '../../images/tabbarLogo/2410615 1.png',
      course_name: 'C ++'
    }],
    article:[{
      id: '0',
      article_image: '../../images/tabbarLogo/2410615 1.png',
      article_name: 'Java',
      advisor_name: 'Su'
    },{
      id: '',
      article_image: '../../images/tabbarLogo/2410615 1.png',
      article_name: 'C ++',
      advisor_name: 'Su'
    },]
  },

  test(){
    wx.navigateTo({
      url: '../../pages/Home/search/search'
    })
  },

  onClose() {
    this.setData({ show: false });
  },

  pa(){
    wx.navigateTo({
      url: '../Article/Article',
    })
  },

  pc(){
    wx.navigateTo({
      url: '../Course/Post/Post',
    })
  },


  navTabbar(e){
    console.log(e.currentTarget.dataset.url)
    wx.reLaunch({
      url: `../${e.currentTarget.dataset.url}/${e.currentTarget.dataset.url}`,
    })
  },
  navPost(){
    this.setData({ show: true });

  },

  onLoad(){
    let id = app.globalData.id
    let token = app.globalData.access_token
    let that = this
    //顾问发布的课程
    wx.request({
      url: api.GetCoursesByCoach + id,
      method: 'GET',
      data:{},
      header:{
        'Authorization': 'Bearer ' + token
      },
      success(res){
        console.log(res)
        if(res.data.status === 0){
           that.setData({
             nocourse: res
           })
        }else if(res.data.status === 1){
          that.setData({
            course: res.data.courses
          })
        }
      }
    })

     //顾问发布的文章
     wx.request({
      url: api.GetArticleByCoach + id,
      method: 'GET',
      data:{},
      header:{
        'Authorization': 'Bearer ' + token
      },
      success(res){
        console.log(res)
        that.setData({
          article: res.data.courses
        })
      }
    })
  },

  onShow(){
    wx.hideHomeButton({
      success: (res) => {},
    })
  },

  goNo(){
    wx.navigateTo({
      url: '../../pages/Home/Notifications/Notifications',
    })
  },

  navMes(options){
    let id = options.currentTarget.dataset.id
    wx.navigateTo({
      url: '../Course/Mes/Mes?id=' +id ,
    })
  }

})