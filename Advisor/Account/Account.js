// Advisor/Account/Account.js
const api = require('../../config/api.js')
const app = getApp()

Page({

  /**
   * 页面的初始数据
   */
  data: {

  },
  onLoad(){
    console.log(getApp().globalData)
    this.setData(getApp().globalData)
    let id = app.globalData.id
    let token = app.globalData.access_token
    let that = this
    wx.request({
      url: api.UserAvatar + id,
      method: 'GET',
      data:{},
      header:{
        'Authorization': 'Bearer ' + token
      },
      success(res){
        console.log(res)
        that.setData({
          avatar: res.data.avatar
        })
      }
    })
  },
  navHome(){
    wx.navigateTo({
      url: '../Home/Home',
    })
  },
  navTabbar(e){
    console.log(e.currentTarget.dataset.url)
    wx.reLaunch({
      url: `../${e.currentTarget.dataset.url}/${e.currentTarget.dataset.url}`,
    })
  },
  navProfile(){
    wx.navigateTo({
      url: '../../pages/Account/Profile/Profile'
    })
  },
  navSettings(){
    wx.navigateTo({
      url: '../../pages/Account/Profile/Settings'
    })
  },
  navDrafts(){
    wx.navigateTo({
      url: './Drafts/Drafts'
    })
  },
  onShareAppMessage(){
    return{
      imageUrl: '../../pages/static/FL.png',
      title: 'Future Leaders',
      path: '../Start/Start'
    }
  },
  share(){
    wx.showShareMenu({
      menus: [],
    })
  },
  navFirst(){
    wx.reLaunch({
      url: '../../pages/Start/Start',
    })
  },
  navPublished(){
    wx.navigateTo({
      url: './Published/Published',
    })
  },
  navLiked(){
    wx.navigateTo({
      url: './LikednShared/LikednShared',
    })
  },
  navFollower(){
    wx.navigateTo({
      url: './Follower/Follower',
    })
  },
  navNotifications(){
    wx.navigateTo({
      url: '../../pages/Home/Notifications/Notifications',
    })
  },

  onShow(){
    wx.hideHomeButton({
      success: (res) => {},
    })
  }


})