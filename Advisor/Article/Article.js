// Advisor/Article/Article.js
const api = require('../../config/api.js')
const app = getApp()

Page({

  /**
   * Page initial data
   */
  data: {
    checked: false,
  },

  onChange({ detail }) {
    // 需要手动对 checked 状态进行更新
    this.setData({ checked: detail });
  },

  savearticle(e){
    let temp = {}
    temp[e.currentTarget.dataset.name] = e.detail.value
    this.setData(temp)
  },


  /**
   * Lifecycle function--Called when page load
   */
  onLoad() {
    let token = app.globalData.access_token
    wx.request({
      url: api.ArticleCategories,
      method:'GET',
      data: {},
      header:{
        'Authorization': 'Bearer ' + token
      },
      success(res){
        console.log(res)
      }
    })
  },
 
  chooseimage(){
    let that = this
    wx.chooseImage({
      count: 9,　　　　　　　　　　　　
      sizeType: ['original', 'compressed'],      
      sourceType: ['album', 'camera'],           
      success: function(res) {
        that.setData({
          imageList:res.tempFilePaths
        })
      },
    })
  },

  save(){
    let id = app.globalData.id
    let token = app.globalData.access_token
    let self = this
    wx.request({
      url: api.SaveArticle,
      method:'POST',
      data: {
        name: self.data.article_name,
        description: self.data.description,
        categories: self.data.categories,
        canreview: self.data.checked,
        postimage: wx.uploadFile({
          filePath: self.data.imageList,
          name: 'file',
        }),
        user_id: id,
        saved: 1
      },
      header:{
        'Authorization': 'Bearer ' + token
      },
      success(res){
        console.log(res)
      }
    })
  },

  /**
   * Lifecycle function--Called when page is initially rendered
   */
  onReady: function () {

  },

  /**
   * Lifecycle function--Called when page show
   */
  onShow: function () {

  },

  /**
   * Lifecycle function--Called when page hide
   */
  onHide: function () {

  },

  /**
   * Lifecycle function--Called when page unload
   */
  onUnload: function () {

  },

  /**
   * Page event handler function--Called when user drop down
   */
  onPullDownRefresh: function () {

  },

  /**
   * Called when page reach bottom
   */
  onReachBottom: function () {

  },

  /**
   * Called when user click on the top right corner to share
   */
  onShareAppMessage: function () {

  }
})