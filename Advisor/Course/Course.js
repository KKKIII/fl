// Advisor/Course/Course.js
const api = require('../../config/api.js')
const app = getApp()


Page({

  /**
   * 页面的初始数据
   */
  data: {
    show: false,
    showconfirmbutton: false,
    favor_img: "../../images/tabbarLogo/Heart.png",
    favor_img2: "../../images/tabbarLogo/like.png"
  },

  onClose() {
    this.setData({ show: false });
  },

  pa(){
    wx.navigateTo({
      url: '../Article/Article',
    })
  },

  pc(){
    wx.navigateTo({
      url: './Post/Post',
    })
  },


  Like1:function(e){
    var that = this;
    var hasChange = that.data.hasChange;
    let token = app.globalData.access_token;
    if(hasChange !== undefined){
       var onum = parseInt(that.data.like);
       console.log(hasChange);
       if(hasChange == 'true'){
         that.data.like = (onum - 1);
         that.data.hasChange = 'false';
         that.data.show = false;
       }else {
         that.data.like = (onum + 1);
         that.data.hasChange = 'true';
         that.data.show = true;
       }
       this.setData({
         like: that.data.like,
         hasChange: that.data.hasChange,
         show: that.data.show
       })
    };
    wx.request({
     url: api.Likearticle,
     method: 'POST',
     data: {
       user_id: wx.getStorageSync('id'),
       article_id: 8
     },
     header:{
       'Authorization': 'Bearer ' + token
     },
     success:function(res){
       console.log(res);
     },
     fail: function(res){
       console.log(res);
     }
   });
   var pages = getCurrentPages();
   if (pages.length > 1){
     var beforePage = pages[pages.length - 2];
     beforePage.changeData();
   }



 },

 goarticle(options){
  let id = options.currentTarget.dataset.id
  var that = this
  wx.navigateTo({
    url: '../../pages/Home/ArticleDetail/ArticleDetail?id=' + id
  })
 },

  navPost(){
    this.setData({ show: true });
  },
  navHome(){
    wx.navigateTo({
      url: '../Home/Home',
    })
  },
  navTabbar(e){
    console.log(e.currentTarget.dataset.url)
    wx.reLaunch({
      url: `../${e.currentTarget.dataset.url}/${e.currentTarget.dataset.url}`,
    })
  },
  navMes(options){
    let id = options.currentTarget.dataset.id
    wx.navigateTo({
      url: './Mes/Mes?id=' +id ,
    })
  },

  onShow(){
    wx.hideHomeButton({
      success: (res) => {},
    })
  },

  onLoad(){
    let id = app.globalData.id
    let token = app.globalData.access_token
    let that = this
    wx.request({
      url: api.HotCourses,
      method: 'GET',
      data: {},
      header:{
        'Authorization': 'Bearer ' + token
      },
      success:function (res){
        console.log(res)
        that.setData({
          courses: res.data.courses
        })
      },
      fail(error){}
    })

         //首页推荐文章
    wx.request({
      url: api.SuggestedArticles,
      method: 'GET',
      data: {},
      header:{
        'Authorization': 'Bearer ' + token
      },
      success:function (res){
        console.log(res)
        that.setData({
          articles: res.data.articles
        })
      },
      fail(error){}
    })

  }

})