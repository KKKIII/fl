// Advisor/Course/Post/Post.js
const api = require('../../../config/api.js')
const app = getApp()


Page({

  /**
   * 页面的初始数据
   */
  data: {
    current: 0,
    videoPath: '',
    videoNmae: 'Upload Video',
    ApproxTitle: '1hours',
    ApproxNames: '',
    ApproxList: [
      '1hours',
      '2hours',
      '3hours',
      '4hours'
    ],
    WeeksNames: '',
    WeeksTitle: '1weeks',
    WeeksList: [
      '1weeks',
      '2weeks',
      '3weeks',
      '4weeks'
    ],
    LanguageNames: '',
    LanguageTitle: 'select',
    LanguageList: [
      'English',
      'Chinese',
      'French',
      'German'
    ],
    IndustryNames: '',
    IndustryTitle: 'select',
    IndustryList: [
      '1',
      '2',
      '3',
      '4'
    ],
    LevelNames: '',
    LevelTitle: 'select',
    LevelList: [
      '5',
      '6',
      '7',
      '8'
    ],
    skill: [],
    switch1: false,
    switch1: false,
    switch1: false,
    switch1: false
  },
  navBack(){
    wx.navigateBack({
      delta: 0
    })
  },
  currentBack(){
    var self = this
    var num = this.data.current
    if(num > 0){
      num -= 1
      self.setData({
        current: num
      })
    }
  },
  forbid() {},
  next() {
    wx.navigateTo({
      url: './Post2',
    })
  },
  onChange(event) {
    console.log(event.currentTarget.dataset.name)
    let name = event.currentTarget.dataset.name
    this.setData({
      [name]: event.detail,
    });
  },
  ApproxSel(event) {
    console.log(event.currentTarget.dataset.sel)
    this.setData({
      ApproxNames: [''],
      ApproxTitle: event.currentTarget.dataset.sel
    });
  },
  WeeksSel(event) {
    console.log(event.currentTarget.dataset.sel)
    this.setData({
      WeeksNames: [''],
      WeeksTitle: event.currentTarget.dataset.sel
    });
  },
  LanguageSel(event){
    console.log(event.currentTarget.dataset.sel)
    this.setData({
      LanguageNames: [''],
      LanguageTitle: event.currentTarget.dataset.sel
    });
  },
  IndustrySel(event){
    console.log(event.currentTarget.dataset.sel)
    this.setData({
      IndustryNames: [''],
      IndustryTitle: event.currentTarget.dataset.sel
    });
  },
  LevelSel(event){
    console.log(event.currentTarget.dataset.sel)
    this.setData({
      LevelNames: [''],
      LevelTitle: event.currentTarget.dataset.sel
    });
  },
  skill(e) {
    if (e.detail.value == '') {

    } else {
      console.log(e.detail.value)
      let list = this.data.skill
      list.push(e.detail.value)
      this.setData({
        skill: list
      })
    }
  },
  skillDel(e) {
    console.log(e.currentTarget.dataset.id)
    let list = this.data.skill
    list.splice(e.currentTarget.dataset.id, 1)
    this.setData({
      skill: list
    })
  },
  changeSwiper(e){
    this.setData({
      current: e.currentTarget.dataset.id
    })
  },
  changeSwiper1(event){
    this.setData({
      current: event.detail.current
    })
  },
  chooseVideo(){
    let that = this
    wx.chooseVideo({
      success(res){
        console.log(res)
        that.setData({
          videoPath: res.tempFilePath,
          videoNmae: 'Selected'
        })
      }
    })
  },
  ChangeSwitch(res){
    let name = res.currentTarget.dataset.name
    this.setData({ [name]: res.detail });
  },
  formSubmit(event) {
    let self = this
    let form = {
      Title: event.detail.value.Title,
      videoPath: self.data.videoPath,
      housr: self.data.ApproxTitle,
      week: self.data.WeeksTitle,
      Course: event.detail.value.Course,
      learn: event.detail.value.learn,
      skill: self.data.skill,
      Syllabus: event.detail.value.Syllabus,
      Notes: event.detail.value.Notes
    }
    console.log(form)
    wx.navigateTo({
      url: `./Preview/Preview?data=${JSON.stringify(form)}`,
    })
  },

  onLoad(){
    let id = app.globalData.id
    let token = app.globalData.access_token
    let self = this
    wx.request({
      url: api.SavaCourse,
      method: 'POST',
      data:{
        advisor_id: id,
        course_name: self.data.course_name,
        course_description: self.data.course_description
      }
    })
  },

  savecourse(e){
    let temp = {}
    temp[e.currentTarget.dataset.name] = e.detail.value
    this.setData(temp)
  },


})