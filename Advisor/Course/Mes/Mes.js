// pages/Advisors/info/courses/courses.js
const api = require('../../../config/api.js')
const app = getApp()


Page({

  /**
   * 页面的初始数据
   */
  data: {
    van_icon: 0,
    navBack: 0,
    id: 0
  },
  videoPlay(){
    let that = this
    let videoplay = wx.createVideoContext('video')
    videoplay.play()
    that.setData({
      van_icon: 1,
      controls: true,
      navBack: 1
    })
  },
  videoStop(){
    let that = this
    that.setData({
      van_icon: 0,
      controls: false,
      navBack: 0
    })
  },
  back(){
    wx.navigateBack({
      delta: 0,
    })
  },
  onLoad(e){
    let that = this
    let token = app.globalData.access_token
    console.log(e)
    that.setData({
      id: e.id
    })
    wx.request({
      url: api.PreviewCourse + e.id,
      method: 'GET',
      data:{},
      header:{
        'Authorization': 'Bearer ' + token
      },
      success(res){
        console.log(res)
        that.setData({
          videosource: res.data.course.videosource,
          course_name: res.data.course.course_name,
          course_description: res.data.course.course_description,
          what_you_will_learn: res.data.course.what_you_will_learn,
          what_skill_will_gain: res.data.course.what_skill_will_gain
        })
      }
    })
  }
})