// pages/Start/Login/Code/Code.js
const api = require('../../../../config/api.js')
import event from '../../../../utils/event'

Page({
  data: {
    change: 0,
    time: 60 * 1000,
    timeData: {},
    Length: 4, //输入框个数
    isFocus: true, //聚焦
    Value: "", //输入的内容
    ispassword: true,
    language: ''
  },
  onLoad(e) {
      // 国际化
      this.setLanguage();  // (1)
      event.on("languageChanged", this, this.setLanguage); // (2)
    let self = this
    console.log(e)
    this.setData(e)
    wx.request({
      url: api.userLogs,
      method: 'POST',
      data: {
        email_id: self.data.email
      },
      success(res){
        console.log(res)
      }
    })
  },

     // 国际化
     setLanguage() {
      this.setData({
        language: wx.T.getLanguage()
      });
     },

  navBack() {
    wx.navigateBack({
      delta: 0
    })
  },
  onChange(e) {
    this.setData({
      timeData: e.detail,
    });
  },
  Focus(e) {
    var that = this;
    console.log(e.detail.value);
    var inputValue = e.detail.value;
    that.setData({
      Value: inputValue,
    })
  },
  Tap() {
    var that = this;
    that.setData({
      isFocus: true,
    })
  },
  wait() {
    wx.showToast({
      title: 'Wait',
      icon: 'error'
    })
  },
  finished() {
    this.setData({
      change: 1
    })
  },
  Resend() {
    let self = this
    wx.request({
      url: api.SignInOTPResend,
      method: 'POST',
      data: {
        email_id: self.data.email
      },
      success(res){
        console.log(res)
        wx.showToast({
          title: 'SUCCESS',
          icon:'success'
        })
      }
    })

  },
  Resend1() {
    let self = this
    wx.request({
      url: api.SignUpOTPResend,
      method: 'POST',
      data: {
        email_id: self.data.email
      },
      success(res){
        console.log(res)
        wx.showToast({
          title: 'SUCCESS',
          icon:'success'
        })
      }
    })

  },

  /**
   * 执行登录逻辑
   */
  submit() {
    const app = getApp()
    const self = this
    console.log(self.data.Value)
    console.log(self.data.email)
    wx.request({
      url: api.userVotp,
      method: 'POST',
      data: {
        email_id: self.data.email,
        otp: self.data.Value
      },
      success(res) {
        console.log('res',res)
        if (res.data.user) {
          getApp().globalData.email = res.data.user.email
          getApp().globalData.firstname = res.data.user.firstname
          getApp().globalData.lastname = res.data.user.lastname
          getApp().globalData.id = res.data.user.id
          getApp().globalData.access_token = res.data.access_token
          getApp().globalData.full_name = res.data.user.full_name
          getApp().globalData.phone_number = res.data.user.phone_number
          let token = app.globalData.access_token
          let id = app.globalData.id
          let email = app.globalData.email
          let full_name = app.globalData.full_name
          wx.setStorageSync('id', res.data.user.id)
          wx.setStorageSync('token', res.data.access_token)
          if(getApp().globalData.role == 'Company'){
            wx.reLaunch({
              url: '../../../../Company/Home/Home',
            })
          }else if(getApp().globalData.role == 'Advisor'){
            wx.reLaunch({
              url: '../../../../Advisor/Signup/Signup',
            })
          }else if(getApp().globalData.role == 'User'){
            wx.reLaunch({
              url: '../../../Home/Home'
            })
          }
        } else if (!res.data.user) {
          console.log('222')
          wx.request({
            url: api.userLogin,
            method: 'POST',
            data: {
              email_id: self.data.email,
              otp: self.data.Value
            },
            success(res){
              console.log('2',res)
              getApp().globalData.email = res.data.user.email
              getApp().globalData.firstname = res.data.user.firstname
              getApp().globalData.lastname = res.data.user.lastname
              getApp().globalData.full_name = res.data.user.full_name
              getApp().globalData.id = res.data.user.id
              getApp().globalData.access_token = res.data.access_token
              getApp().globalData.phone_number = res.data.user.phone_number
              wx.setStorageSync('token', res.data.access_token)
              wx.setStorageSync('id', res.data.user.id)
              if(getApp().globalData.role == 'Company'){
                wx.navigateTo({
                  url: '../../../../Company/Home/Home',
                })
              }else if(getApp().globalData.role == 'Advisor'){
                wx.reLaunch({
                  url: '../../../../Advisor/Home/Home',
                })
              }else if(getApp().globalData.role == 'User'){
                wx.reLaunch({
                  url: '../../../Home/Home',
                })
              }
            }
          })
        }
      }
    })
  }
})