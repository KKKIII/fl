// pages/Start/Login/test.js
const api = require('../../../config/api.js')
import event from '../../../utils/event'

Page({

  /**
   * 页面的初始数据
   */
  data: {
    language: '',
    show: true,
    showconfirmbutton: false,
  },
  
  onClose() {
    this.setData({ show: false });
  },

  onLoad(){
    // 国际化
    this.setLanguage();  // (1)
    event.on("languageChanged", this, this.setLanguage); // (2)
  },
 
   // 国际化
 setLanguage() {
  this.setData({
    language: wx.T.getLanguage()
  });
 },

  nav(e){
    getApp().globalData.role = e.currentTarget.dataset.name
    if(e.currentTarget.dataset.name == 'Advisor'){
      api.usersignup = 'https://futureleaderstech.com/api/auth/adivsorsignup'
      api.userVotp = 'https://futureleaderstech.com/api/auth/verifyadvisorsignup'
      api.userLogs = 'https://futureleaderstech.com/api/auth/advisorlogin'
      api.userLogin = 'https://futureleaderstech.com/api/auth/verifyadvisorlogin'
      console.log(api)
    }else if(e.currentTarget.dataset.name == 'Company'){
      api.usersignup = 'https://futureleaderstech.com/api/auth/enterprisesignup'
      api.userVotp = 'https://futureleaderstech.com/api/auth/verifyenterprisesignup'
      api.userLogs = 'https://futureleaderstech.com/api/auth/enterpriselogin'
      api.userLogin = 'https://futureleaderstech.com/api/auth/verifyenterpriselogin'
      console.log(api)
    }
    wx.navigateTo({
      url: './sign',
    })
  }
})

