// pages/Start/Login/AccLog.js
import event from '../../../utils/event'


Page({

  /**
   * 页面的初始数据
   */
  data: {
    language: ''

  },

  onLoad(){
        // 国际化
        this.setLanguage();  // (1)
        event.on("languageChanged", this, this.setLanguage); // (2)
  },

     // 国际化
     setLanguage() {
      this.setData({
        language: wx.T.getLanguage()
      });
     },

  navBack(){
    wx.navigateBack({
      delta: 0
    })
  },
  login(){
    let self = this
    wx.navigateTo({
      url: `./Code/Code?email=${self.data.email}`,
    })
  },
  navSign(){
    wx.navigateBack({
      delta: 0
    })
  },
  getValue(e){
    let temp = {}
    temp[e.currentTarget.dataset.name] = e.detail.value
    this.setData(temp)
  }
})