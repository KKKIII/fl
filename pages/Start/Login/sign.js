// pages/Start/Login/Login.js
const api = require('../../../config/api.js')
import event from '../../../utils/event'


Page({
  data: {
    iconLogo: '',
    account: '', //账号
    nickname: '', // 昵称
    password: '', //密码
    // isRegister: false,//登录菊花,
    errorMessage: '', //提示错误信息
    //电话号码前缀
    index: 0,
    array: ['user', 'advisor', 'enterprise'],
    show: false,
    display: 'none', 
    role: 'user',
    language: '',
    phone_number: '',
    countries:[],
    code: '+86 '
  },

  bindPickerChange: function (e) {
    console.log('picker发送选择改变，携带值为', e.detail.value) 
    console.log('picker发送选择改变，携带值为', this.data.countries[e.detail.value].code) 
    let that = this
    that.setData({
      code: this.data.countries[e.detail.value].code
    })
  },

  onLoad(){
    let that = this
    var me = this
    console.log(getApp().globalData.role)
        // 国际化
        this.setLanguage();  // (1)
        event.on("languageChanged", this, this.setLanguage); // (2)

        wx.request({
          url: api.GetCountryAndCodes,
          method: 'GET',
          data: {},
          header: {},
          success(res){
            console.log(res)
            that.setData({
              countries: res.data.countries
            })
          }
        })   
  },


     // 国际化
 setLanguage() {
  this.setData({
    language: wx.T.getLanguage()
  });
 },


  /**
   * 单击登录,跳转到注册页
   */
  registerLoginTap: function () {
    wx.navigateBack({
      url: '../login/login',
    })
  },
  //返回前一页
  navBack() {
    wx.navigateBack({
      delta: 0
    })
  },
  disShow(){
    this.setData({
      display: 'inline'
    })
  },
  cancel(){
    this.setData({
      display: 'none'
    })
  },
  confirm(event){
    const { picker, value, index } = event.detail;
    console.log(value)
    this.setData({
      role: value,
      display: 'none'
    })
  },
  inputHandler(e){
    let temp = {}
    temp[e.currentTarget.dataset.type] = e.detail.value
    this.setData(temp)    //去除{}
  },
  //跳转界面
  navlog() {
    wx.navigateTo({
      url: './AccLog',
    })
    // wx.navigateTo({
    //   url: './AccLog'
    // })
  },
  signUp(){
    let self = this
    wx.request({
      url: api.usersignup,
      method: 'POST',
      data:{
        full_name: this.data.full_name,
        email: this.data.email,
        phone_number: this.data.code + this.data.phone_number,
        secret_key: 'userRegisterFL'
      },
      success(res){
        console.log('1',res)
        if(res.data.status){
          console.log(self.data)
          wx.navigateTo({
            url: `./Code/Code?full_name=${self.data.full_name}&phone_number=${self.data.code + self.data.phone_number}&email=${self.data.email}`,
          })
        }else{
          wx.showToast({
            title: 'Fill in completely',
            icon: 'error'
          })
        }
      }
    })
  },
  Term(){
     wx.navigateTo({
       url: '../../../other/Terms/Terms',
     })
  },
  Privacy(){
    wx.navigateTo({
      url: '../Privacy/Privacy',
    })
 }
})