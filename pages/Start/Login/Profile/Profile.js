// pages/Start/Login/Profile/Profile.js

const api = require('../../../../config/api.js')
const app = getApp()


Page({

  /**
   * 页面的初始数据
   */
  data: {
    Resume_fileName: '',
    Resume_videoName: '',
    Resume_file: [],
    Resume_video: [],

    dob:'',
    array: ['Higher Education','Graduate','Post Graduate','PHD'],
    objectArray: [
      {
        id: 0,
        name: 'Higher Education'
      },
      {
        id: 1,
        name: 'Graduate'
      },
      {
        id: 2,
        name: 'Post Graduate'
      },
      {
        id: 3,
        name: 'PHD'
      }
   ],
   education: '',
  industries: '',
  industries1: [],

 job_roles: '',
 job_roles1: [],
locations: '',
locations1:[],
array4: ['Jobseeking','Upskilling','Both'],
objectArray4: [
  {
    id: 0,
    name: 'Jobseeking'
  },
  {
    id: 1,
    name: 'Upskilling'
  },
  {
    id: 2,
    name: 'Both'
  }
],
training_goals: '',
training_goals1: [],
phone_number: app.globalData.phone_number,
language: '',
language1: []
  },

  bindDateChange: function(e) {
    console.log('picker发送选择改变，携带值为', e.detail.value)
    this.setData({
      dob: e.detail.value
    })
  },

  bindPickerChange: function (e) {
    console.log('picker发送选择改变，携带下标为', e.detail.value)
    console.log('picker发送选择改变，携带值为', this.data.array[e.detail.value])
    this.setData({
      education: this.data.array[e.detail.value]
    })
  },

  bindPickerChange1: function (e) {
    console.log('picker发送选择改变，携带下标为', e.detail.value)
    console.log('picker发送选择改变，携带值为', this.data.industries1[e.detail.value].industry_name)
    this.setData({
      industries: this.data.industries1[e.detail.value].industry_name
    })
  },

  bindPickerChange2: function (e) {
    console.log('picker发送选择改变，携带下标为', e.detail.value)
    console.log('picker发送选择改变，携带值为', this.data.job_roles1[e.detail.value].job_role)
    this.setData({
      job_roles: this.data.job_roles1[e.detail.value].job_role
    })
  },

  bindPickerChange3: function (e) {
    console.log('picker发送选择改变，携带下标为', e.detail.value)
    console.log('picker发送选择改变，携带值为', this.data.locations1[e.detail.value].location_name)
    this.setData({
      locations: this.data.locations1[e.detail.value].location_name
    })
  },

  bindPickerChange4: function (e) {
    console.log('picker发送选择改变，携带下标为', e.detail.value)
    console.log('picker发送选择改变，携带值为', this.data.training_goals1[e.detail.value].training_name)
    this.setData({
      training_goals: this.data.training_goals1[e.detail.value].training_name
    })
  },

  bindPickerChange5: function (e) {
    console.log('picker发送选择改变，携带下标为', e.detail.value)
    console.log('picker发送选择改变，携带值为', this.data.language1[e.detail.value].language)
    this.setData({
      language: this.data.language1[e.detail.value].language
    })
  },
  /**
   * 生命周期函数--监听页面加载
   */
  onLoad () {
    console.log(getApp().globalData)
    
    this.setData(getApp().globalData)
    let that = this
    let id = app.globalData.id
    let token = app.globalData.access_token
    wx.request({
      url: api.UserAvatar + id,
      method: 'GET',
      data:{},
      header:{
        'Authorization': 'Bearer ' + token
      },
      success(res){
        console.log(res)
        that.setData({
          avatar1: res.data.avatar
        })
      }
    }),

    wx.request({
      url: api.GetUserCompletedProfile + id,
      method: 'GET',
      data:{},
      header:{
        'Authorization': 'Bearer ' + token
      },
      success(res){
        console.log(res)
        that.setData({
          education:res.data.profile.education,
          address: res.data.profile.address,
          age: res.data.profile.age,
          city: res.data.profile.city,
          dob: res.data.profile.dob,
          industries: res.data.profile.industries,
          job_roles: res.data.profile.job_roles,
          languages: res.data.profile.languages,
          locations: res.data.profile.locations,
          postalcode: res.data.profile.postalcode,
          school_university: res.data.profile.school_university,
          state: res.data.profile.state,
          tag_line: res.data.profile.tag_line,
          training_goals: res.data.profile.training_goals,
          yearofgraduate: res.data.profile.yearofgraduate,
          phone_number: app.globalData.phone_number
        })
      }
    })

    wx.request({
      url: api.OptionsOfLanguages,
      method: 'GET',
      data: {},
      header: {},
      success(res){
        console.log(res)
        that.setData({
          language1: res.data.languages
        })
      }
    })

    wx.request({
      url: api.OptionsOfIndustries,
      method: 'GET',
      data: {},
      header: {},
      success(res){
        console.log(res)
        that.setData({
          industries1: res.data.industries
        })
      }
    })

    wx.request({
      url: api.OptionsOfLocations,
      method: 'GET',
      data: {},
      header: {},
      success(res){
        console.log(res)
        that.setData({
          locations1: res.data.locations
        })
      }
    })

    wx.request({
      url: api.OptionsOfTrainingGoals,
      method: 'GET',
      data: {},
      header: {},
      success(res){
        console.log(res)
        that.setData({
          training_goals1: res.data.traininggoals
        })
      }
    })

    wx.request({
      url: api.GetJobRoles,
      method: 'GET',
      data: {},
      header: {},
      success(res){
        console.log(res)
        that.setData({
          job_roles1: res.data.roles
        })
      }
    })

    wx.request({
      url: api.Getexperiences + id,
      method: 'GET',
      data: {},
      header:{
        'Authorization': 'Bearer ' + token,
      },
      success(res){
        console.log(res)
        that.setData({
          year_of_experience:res.data.experiences[0].no_of_years
        })
      }
    })

    
    wx.request({
      url: api.UserAbout + id,
      method: 'GET',
      data: {
      },
      header:{
        'Authorization': 'Bearer ' + token
      },
      success:function (res){
        console.log(res)
        that.setData({
          userinfo: res.data.aboutInfo.about_text
        })
      },
      fail(error){}
    })

    wx.request({
      url: api.GetUserContacts + id,
      method: 'GET',
      data:{},
      header:{
        'Authorization': 'Bearer ' + token
      },
      success(res){
        console.log(res)
        that.setData({
          linkedin_profile: res.data.contacts.linkedin_profile,
        })
      }
    })


  },
  
  getphonenumber:function(e){
    console.log(e)
    this.setData({
      phone: e.detail.value
    })
  },



  getdob:function(e){
    console.log(e)
    this.setData({
      dob: e.detail.value
    })
  },

  getage:function(e){
    console.log(e)
    this.setData({
      age: e.detail.value
    })
  },

  getaddress:function(e){
    console.log(e)
    this.setData({
      address: e.detail.value
    })
  },

  getabout:function(e){
    console.log(e)
    this.setData({
      about: e.detail.value
    })
  },
  
  geteducation:function(e){
    console.log(e)
    this.setData({
      education: e.detail.value
    })
  },

  getyearofgraduate:function(e){
    console.log(e)
    this.setData({
      yearofgraduate: e.detail.value
    })
  },

  getschooloruniversity:function(e){
    console.log(e)
    this.setData({
      school_university: e.detail.value
    })
  },

  getyearofexperience:function(e){
    console.log(e)
    this.setData({
      yearofgraduate: e.detail.value
    })
  },

  getlanguage:function(e){
    console.log(e)
    this.setData({
      language: e.detail.value
    })
  },

  getindustry:function(e){
    console.log(e)
    this.setData({
      industries: e.detail.value
    })
  },

  getjobrole:function(e){
    console.log(e)
    this.setData({
      job_roles: e.detail.value
    })
  },

  getlocation:function(e){
    console.log(e)
    this.setData({
      locations: e.detail.value
    })
  },

  gettraininggoals:function(e){
    console.log(e)
    this.setData({
      training_goals: e.detail.value
    })
  },

  postresume:function(){
    wx.request({
      url: api.GetSubResumes + id,
      method: 'POST',
      data:{
        resumefile: e.detail.value
      },
      header:{
        'Authorization': 'Bearer' + token
      },
      success:function(res){

      }
    })
  },

  getlinkedin:function(e){
    console.log(e)
    this.setData({
      linkedin: e.detail.value
    })
  },

  getemail:function(e){
    console.log(e)
    this.setData({
      email: e.detail.value
    })
  },

  gohome(e){
    let that = this
    let id = app.globalData.id
    let token = app.globalData.access_token
    wx.request({
      url: api.UserProfileAdd + id ,
      method: 'POST',
      data:{
        user_id: id,
        tag_line: '123',
        education: '123',
        industries: '123',
        job_roles: '2123',
        locations: '2132',
        training_goals: '2123',
        dob: '',
        age: '',
        address: '',
        city: '',
        state: '',
        postalcode: '',
        yearofgraduate: '',
        school_university: '',
      },
      header:{
        'Authorization': 'Bearer ' + token,
        "Content-Type": "application/x-www-form-urlencoded"
      },
      success:function(res){
        console.log(res)
      }
    })

 
  },

  /**
   * 生命周期函数--监听页面初次渲染完成
   */
  onReady: function () {

  },

  /**
   * 生命周期函数--监听页面显示
   */
  onShow: function () {

  },

  /**
   * 生命周期函数--监听页面隐藏
   */
  onHide: function () {

  },

  /**
   * 生命周期函数--监听页面卸载
   */
  onUnload: function () {

  },

  /**
   * 页面相关事件处理函数--监听用户下拉动作
   */
  onPullDownRefresh: function () {

  },

  /**
   * 页面上拉触底事件的处理函数
   */
  onReachBottom: function () {

  },

  /**
   * 用户点击右上角分享
   */
  onShareAppMessage: function () {

  },

  changeprofile(e){
    let temp = {}
    temp[e.currentTarget.dataset.name] = e.detail.value
    this.setData(temp)
  },

  postprofile(){
    let self = this
    let token = app.globalData.access_token
    let id = app.globalData.id
    let email = app.globalData.email
    wx.request({
      url: api.UserProfileAdd + id ,
      method: 'POST',
      data:{
        user_id: id,
        tag_line: '123',
        education: self.data.education,
        industries: self.data.industries,
        job_roles: self.data.job_roles,
        locations: self.data.locations,
        training_goals: self.data.training_goals,
        dob: self.data.dob,
        age: self.data.age,
        address: self.data.address,
        city: self.data.city,
        state: self.data.state,
        postalcode: self.data.postalcode,
        yearofgraduate: self.data.yearofgraduate,
        school_university: self.data.school_university,
      },
      header:{
        'Authorization': 'Bearer ' + token,
        "Content-Type": "application/x-www-form-urlencoded"
      },
      success:function(res){
        console.log(res)
        wx.switchTab({
          url: '../../../Account/Account',
        })
      }   
    })

    wx.request({
      url: api.UserContacts,
      method: 'POST',
      data:{
        user_id: id,
        linkedin_profile: self.data.LinkedIn,
        email_id: email
      },
      header:{
        'Authorization': 'Bearer ' + token,
        "Content-Type": "application/x-www-form-urlencoded"
      },
      success:function(res){
        console.log(res)
      }   
    })

    wx.request({
      url: api.Getexperiences + id,
      method: 'POST',
      data: {
        no_of_years: self.data.year_of_experience
      },
      header:{
        'Authorization': 'Bearer ' + token,
        "Content-Type": "application/x-www-form-urlencoded"
      },
      success:function(res){
        console.log(res)
      }   
    })

    wx.request({
      url: api.UserAbout + id ,
      method: 'POST',
      data:{
          about_text: self.data.about_text
      },
      header:{
        'Authorization': 'Bearer ' + token,
        "Content-Type": "application/x-www-form-urlencoded"
      },
      success:function(res){
        console.log(res)
      }
      
    })

  }

  

})