// pages/Account/Account.js
const api = require('../../config/api.js')
var app = getApp()

import event from '../../utils/event'


Page({

  /**
   * 页面的初始数据
   */
  data: {
    email: '',
    language: ''
  },
  navFirst(){
    wx.reLaunch({
      url: '../Start/Start',
    })
  },
  onLoad(){
    console.log(getApp().globalData)
    
    this.setData(getApp().globalData)
    let token = app.globalData.access_token
    let id = app.globalData.id
    let that = this

        // 国际化
        this.setLanguage();  // (1)
        event.on("languageChanged", this, this.setLanguage); // (2)

    wx.request({
      url: api.UserAvatar + id,
      method: 'GET',
      data:{},
      header:{
        'Authorization': 'Bearer ' + token
      },
      success(res){
        console.log(res)
        that.setData({
          avatar: res.data.avatar
        })
      }
    })
  },

// 国际化
setLanguage() {
  this.setData({
    language: wx.T.getLanguage()
  });
},




  navProfile(){
    wx.navigateTo({
      url: './Profile/Profile'
    })
  },
  navSettings(){
    wx.navigateTo({
      url: './Profile/Settings'
    })
  },
  navSaveJobs(){
    wx.navigateTo({
      url: './Profile/SaveJobs'
    })
  },
  navInfo(e){
    console.log(e.currentTarget.dataset.name)
    wx.navigateTo({
      url: `./Profile/${e.currentTarget.dataset.name}`,
    })
  },
  onShareAppMessage(){
    return{
      imageUrl: '../static/lgoo.png',
      title: 'Future Leaders',
      path: '../Start/Start'
    }
  },
  share(){
    wx.showShareMenu({
      menus: [],
    })
  },
  onShareTimeline(res) {
    return {
      title: 'Futrue Leaders',
      path:'../Start/Start'
    }
  },
  navHome(){
    wx.switchTab({
      url: '../Home/Home',
    })
  },
  Certificates(){
    wx.navigateTo({
      url: './Certificates/Certificates',
    })
  },
  Courses(){
    wx.navigateTo({
      url: './Courses/Courses',
    })
  },
  Alerts(){
    wx.navigateTo({
      url: './Alerts/Alerts',
    })
  },
  LikedShared(){
    wx.navigateTo({
      url: './LikedShared/LikedShared',
    })
  },
  Following(){
    wx.navigateTo({
      url: './Following/Following',
    })
  },
  Login(){
    wx.reLaunch({
      url: '../Start/Start',
    })
  }
})