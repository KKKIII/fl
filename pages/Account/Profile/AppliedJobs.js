// pages/Account/Profile/SaveJobs.js
const api = require('../../../config/api.js')
const app = getApp()

Page({

  /**
   * 页面的初始数据
   */
  data: {
    jobs: []
  },
  navBack(){
    wx.navigateBack({
      delta: 0
    })
  },
  clearAll(){
    this.setData({jobs: ''})
  },
  onLoad(){
    let that = this
    let token = app.globalData.access_token
    let id = app.globalData.id
    wx.request({
      url: api.GetAppliedJobs + id,
      method: 'GET',
      header: {
        'Authorization': 'Bearer ' + token
      },
      success(res){
        console.log(res)
      
      }
    })
  }
})