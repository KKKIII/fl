// pages/Account/Profile/Profile.js
const api = require('../../../config/api.js')
var app = getApp()

import event from '../../../utils/event'

Page({

  /**
   * 页面的初始数据
   */
  data: {
    imgSrc: '',
    textarea: `Recent Graduate. Attaining a degree with Merit in 
    3D Computer Animation. Specialising in technical 
    animation, character modelling and texturing.
    Highly skilled in Maya, Zbrush, Substance Painter, 
    Marvelous Designer, Nuke and After Effects. 
    Alongside intermediate experience with Houdini.
    Currently working for Universal Studios in 
    Singapore as a Technical Animator, with a focus on 
    shows and attractions.`,
    Resume_fileName: '',
    Resume_videoName: '',
    Resume_file: [],
    Resume_video: [],
    Experience1: [],
    Experience2: {
      "info": '',
      "name": '',
      "time": ''
    },
    value: '',
    recordsList: [],
    imgs: [],
    language: ''
  },
  onLoad() {
    console.log(getApp().globalData.id)
    console.log(getApp().globalData.access_token)
    var self = this
    var that = this
    let id = app.globalData.id
    let token = app.globalData.access_token
    this.setData(getApp().globalData)
    // 国际化
    this.setLanguage();  // (1)
    event.on("languageChanged", this, this.setLanguage); // (2)
    //获取用户信息
    wx.request({
      url: api.UserAbout + id,
      method: 'GET',
      data: {
      },
      header:{
        'Authorization': 'Bearer ' + token
      },
      success:function (res){
        console.log(res)
        that.setData({
          userinfo: res.data.aboutInfo.about_text
        })
      },
      fail(error){}
    })

    //获取简历
    wx.request({
      url: api.GetSubResumes + id,
      method: 'GET',
      header:{
        'Authorization': 'Bearer ' + token
      },
      success(res) {
        console.log(res)
        that.setData({
          resume: res.data.resumes
        })
      }
    })

    wx.request({
      url: api.GetUserCompletedProfile + id,
      method: 'GET',
      data:{},
      header:{
        'Authorization': 'Bearer ' + token
      },
      success(res){
        console.log(res)
        self.setData({
          education:res.data.profile.education,
          address: res.data.profile.address,
          age: res.data.profile.age,
          city: res.data.profile.city,
          dob: res.data.profile.dob,
          industries: res.data.profile.industries,
          job_roles: res.data.profile.job_roles,
          languages: res.data.profile.languages,
          locations: res.data.profile.locations,
          postalcode: res.data.profile.postalcode,
          school_university: res.data.profile.school_university,
          state: res.data.profile.state,
          tag_line: res.data.profile.tag_line,
          training_goals: res.data.profile.training_goals,
          year_of_experience: res.data.profile.year_of_experience,
          yearofgraduate: res.data.profile.yearofgraduate
        })
      }
    })
  
    wx.request({
      url: api.UserAvatar + id,
      method: 'GET',
      data:{},
      header:{
        'Authorization': 'Bearer ' + token
      },
      success(res){
        console.log(res)
        that.setData({
          avatar1: res.data.avatar
        })
      }
    })
   
    wx.request({
      url: api.GetUserContacts + id,
      method: 'GET',
      data:{},
      header:{
        'Authorization': 'Bearer ' + token
      },
      success(res){
        console.log(res)
        that.setData({
          linkedin_profile: res.data.contacts.linkedin_profile,
          email_id: res.data.contacts.email_id
        })
      }
    })

    wx.request({
      url: api.Getexperiences + id,
      method: 'GET',
      data:{},
      header:{
        'Authorization': 'Bearer ' + token
      },
      success(res){
        console.log(res)
        that.setData({
          experiences: res.data.experiences
        })
      }
    })
    
  },

// 国际化
setLanguage() {
  this.setData({
    language: wx.T.getLanguage()
  });
},



  navBack() {
    wx.navigateBack({
      delta: 0
    })
  },
  chooseFile() {
    var that = this
    console.log('1')
    wx.chooseMessageFile({
      count: 1,
      type: "file",
      success(res) {
        console.log(res.tempFiles[0])
        var name = res.tempFiles[0].name
        if (name.indexOf('.doc') == -1) {
          wx.showToast({
            title: 'Must doc file',
            icon: "error"
          })
        } else {
          wx.request({
            url: 'https://futureleaderstech.com/api/user/resumes/' + id ,
            method:'POST',
            data:{
              resumefile: res.tempFiles[0],
              "Content-Type": "application/form-data"
            },
            header:{
              'Authorization': 'Bearer ' + token,
            },
            success(res){
              console.log(res)
            }
          })
        }
      }
    })
  },
  chooseVideo() {
    var that = this
    wx.chooseMessageFile({
      count: 1,
      type: "video",
      success(res) {
        console.log(res.tempFiles[0].path)
        that.setData({
          Resume_video: res.tempFiles[0].path,
          Resume_videoName: res.tempFiles[0].name
        })
      }
    })
  },
  //工作经验
  ExpChooseImg1() {
    wx.chooseImage({
      count: 1,
      success(res) {
        console.log(res)
      }
    })
  },
  exp1Info(e) {
    let name = 'Experience1.info'
    this.setData({
      [name]: e.detail.value
    })
  },
  exp1Name(e) {
    let name = 'Experience1.name'
    this.setData({
      [name]: e.detail.value
    })
  },
  exp1Time(e) {
    let name = 'Experience1.time'
    this.setData({
      [name]: e.detail.value
    })
  },
  exp2Info(e) {
    let name = 'Experience2.info'
    this.setData({
      [name]: e.detail.value
    })
  },
  exp2Name(e) {
    let name = 'Experience2.name'
    this.setData({
      [name]: e.detail.value
    })
  },
  exp2Time(e) {
    let name = 'Experience2.time'
    this.setData({
      [name]: e.detail.value
    })
  },
  uploadPhoto() {
    var that = this; 
    let id = app.globalData.id
    let token = app.globalData.access_token
    
    wx.chooseImage({
      count: 1,
      success(res) {
        console.log(res.tempFilePaths[0])
        const tempfile = res.tempFilePaths[0]
        wx.uploadFile({
          filePath: res.tempFilePaths[0],
          name: 'file',
          url: api.UserAvatar + id,
          header:{
            "Content-Type": 'multipart/form-data',
            'Authorization': 'Bearer ' + token
          },
          formData:{
            user_id : id,
            avatar: res.tempFilePaths[0]
          },
          success(res){
            console.log(res)
          }
        })
      }
    })
  },
 
  


  updateabout(){
    wx.navigateTo({
      url: '../About/About',
    })
  },

  goupdate(){
    wx.navigateTo({
      url: '../../Start/Login/Profile/Profile',
    })
  }

})