// pages/Account/About/About.js
const api = require('../../../config/api.js')
const app = getApp()


Page({

  /**
   * 页面的初始数据
   */
  data: {

  },

  /**
   * 生命周期函数--监听页面加载
   */
  onLoad () {

  },

  changeprofile(e){
    let temp = {}
    temp[e.currentTarget.dataset.name] = e.detail.value
    this.setData(temp)
  },

  postprofile(){
    let self = this
    let token = app.globalData.access_token
    let id = app.globalData.id
    wx.request({
      url: api.PostaboutUser + id ,
      method: 'POST',
      data:{
          about_text: self.data.about_text
      },
      header:{
        'Authorization': 'Bearer ' + token,
        "Content-Type": "application/x-www-form-urlencoded"
      },
      success:function(res){
        console.log(res)
        wx.reLaunch({
          url: '../Profile/Profile',
        })
      }
      
    })
  },

  /**
   * 生命周期函数--监听页面初次渲染完成
   */
  onReady: function () {

  },

  /**
   * 生命周期函数--监听页面显示
   */
  onShow: function () {

  },

  /**
   * 生命周期函数--监听页面隐藏
   */
  onHide: function () {

  },

  /**
   * 生命周期函数--监听页面卸载
   */
  onUnload: function () {

  },

  /**
   * 页面相关事件处理函数--监听用户下拉动作
   */
  onPullDownRefresh: function () {

  },

  /**
   * 页面上拉触底事件的处理函数
   */
  onReachBottom: function () {

  },

  /**
   * 用户点击右上角分享
   */
  onShareAppMessage: function () {

  }
})