// pages/Account/Alerts/Alerts.js
const api = require('../../../config/api.js')
var app = getApp()


Page({

  /**
   * 页面的初始数据
   */
  data: {
    change: false, // 当两个slider在最右端重合时，将change设置为true，从而隐藏slider2，才能继续操作slider1
    max: 200, // 两个slider所能达到的最大值
    min: 20, // 两个slider所能取的最小值
    rate: 1.8, // slider的最大最小值之差和100（或1000）之间的比率
    slider1Max: 200, // slider1的最大取值
    slider1Value: 20, // slider1的值
    slider2Value: 200, // slider2的值
    slider2Min: 20, // slider2的最小取值
    slider1W: 100, // slider1的宽度
    slider2W: 0, // slider2的宽度
    locations: 'Select / Auto suggest',
    locations1:[],
    industries: 'Select Industry',
    industries1: [],
    categories:'Selected Job Category',
    categories1: []
  },

  create(){
    wx.switchTab({
      url: '../Account',
    })
  },

  bindPickerChange: function (e) {
    console.log('picker发送选择改变，携带下标为', e.detail.value)
    console.log('picker发送选择改变，携带值为', this.data.locations1[e.detail.value].location_name)
    this.setData({
      locations: this.data.locations1[e.detail.value].location_name
    })
  },

  bindPickerChange1: function (e) {
    console.log('picker发送选择改变，携带下标为', e.detail.value)
    console.log('picker发送选择改变，携带值为', this.data.industries1[e.detail.value].industry_name)
    this.setData({
      industries: this.data.industries1[e.detail.value].industry_name
    })
  },

  bindPickerChange2: function (e) {
    console.log('picker发送选择改变，携带下标为', e.detail.value)
    console.log('picker发送选择改变，携带值为', this.data.categories1[e.detail.value].category_name)
    this.setData({
      categories: this.data.categories1[e.detail.value].category_name
    })
  },
 // 开始滑动
 changeStart: function (e) {
  var idx = parseInt(e.currentTarget.dataset.idx)
  if (idx === 1) {
    // dW是当前操作的slider所能占据的最大宽度百分数
    var dW = (this.data.slider2Value - this.data.min) / this.data.rate
    this.setData({
      slider1W: dW,
      slider2W: 100 - dW,
      slider1Max: this.data.slider2Value,
      slider2Min: this.data.slider2Value,
      change: false
    })
  } else if (idx === 2) {
    var dw = (this.data.max - this.data.slider1Value) / this.data.rate
    this.setData({
      slider2W: dw,
      slider1W: 100 - dw,
      slider1Max: this.data.slider1Value,
      slider2Min: this.data.slider1Value,
      change: false
    })
  }
},

// 正在滑动
changing: function (e) {
  var idx = parseInt(e.currentTarget.dataset.idx)
  var value = e.detail.value
  if (idx === 1) {
    this.setData({
      slider1Value: value
    })
  } else if (idx === 2) {
    this.setData({
      slider2Value: value,
    })
  }
},
changed: function (e) {
  console.log(e)
  if (this.data.slider1Value === this.data.slider2Value && this.data.slider2Value === this.data.max) {
    this.setData({
      change: true
    })
  }
},



  /**
   * 生命周期函数--监听页面加载
   */
  onLoad () {
    let that = this
    let id = app.globalData.id
    let token = app.globalData.access_token

    wx.request({
      url: api.OptionsOfLocations,
      method: 'GET',
      data: {},
      header: {},
      success(res){
        console.log(res)
        that.setData({
          locations1: res.data.locations
        })
      }
    })

    wx.request({
      url: api.OptionsOfIndustries,
      method: 'GET',
      data: {},
      header: {},
      success(res){
        console.log(res)
        that.setData({
          industries1: res.data.industries
        })
      }
    })

    wx.request({
      url: api.GetJobCategories,
      method: 'GET',
      data: {},
      header:{
        'Authorization': 'Bearer ' + token
      },
      success:function (res){
        console.log(res)
        that.setData({
          categories1: res.data.categories
        })
      },
      fail(error){}
    })

  },

  /**
   * 生命周期函数--监听页面初次渲染完成
   */
  onReady: function () {

  },

  /**
   * 生命周期函数--监听页面显示
   */
  onShow: function () {

  },

  /**
   * 生命周期函数--监听页面隐藏
   */
  onHide: function () {

  },

  /**
   * 生命周期函数--监听页面卸载
   */
  onUnload: function () {

  },

  /**
   * 页面相关事件处理函数--监听用户下拉动作
   */
  onPullDownRefresh: function () {

  },

  /**
   * 页面上拉触底事件的处理函数
   */
  onReachBottom: function () {

  },

  /**
   * 用户点击右上角分享
   */
  onShareAppMessage: function () {

  }
})