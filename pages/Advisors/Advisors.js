// pages/Advisors/Advisors.js
const api = require('../../config/api.js')
const app = getApp()

import event from '../../utils/event'

Page({

  /**
   * 页面的初始数据
   */
  data: {
    active: 0,
    category:[
      "All",
      "Personality",
      "Leadership",
      "Category",
      "All",
      "Personality",
      "Leadership",
      "Category"
    ],
    Advisors: [""],
    showLength:5,
    advisors:[{
      profile_image: "../../images/tabbarLogo/Group 19994.png",
      lastname:"wang",
      firstname: "q",
      likes: "15"
    }],
    courses:[{
      id: "1",
      course_image: "../../images/tabbarLogo/Group 19994.png",
      course_name: "JAVA",
      course_industry: "IT"
    }],
    language: ''
  },
  select(e){
    let that = this
    console.log(e.currentTarget.dataset.id)
    that.setData({
      active: e.currentTarget.dataset.id
    })
  },
  navMes(options){
    let id = options.currentTarget.dataset.id
    wx.navigateTo({
      url: './info/info?id=' + id
    })
  },
  navHome(){
    wx.switchTab({
      url: '../Home/Home',
    })
  },
  onShareAppMessage(){
    return{
      imageUrl: '../static/lgoo.png',
      title: 'Future Leaders',
      path: '../Start/Start'
    }
  },
  share(){
    wx.showShareMenu({
      menus: [],
    })
  },
  onShareTimeline(res) {
    return {
      imageUrl:'../static/tabbarLogo/lgoo.png',
      title: 'Futrue Leaders',
      path:'../Start/Start'
    }
  },
  onLoad(){
    console.log(getApp().globalData.id)
    console.log(getApp().globalData.access_token)
    this.setData(getApp().globalData)
    var that = this
    let token = app.globalData.access_token
        // 国际化
this.setLanguage();  // (1)
event.on("languageChanged", this, this.setLanguage); // (2)
    //顾问
    wx.request({
      url: api.ExploreAdvisors,
      method: 'GET',
      data: {},
      header:{
        'Authorization': 'Bearer ' + token
      },
      success:function (res){
        console.log(res)
        wx.setStorageSync('coach_id', res.data.advisors[0].id)
        that.setData({
          advisors: res.data.advisors
        })
      },
      fail(error){}
    })
        //顾问明细
        wx.request({
          url: api.ViewAdvisor + wx.getStorageSync('coach_id'),
          method: 'GET',
          data: {},
          header:{
            'Authorization': 'Bearer ' + token
          },
          success:function (res){
            console.log(res)
            that.setData({
            })
          },
          fail(error){}
        })
    //工作分类
    wx.request({
      url: api.GetJobCategories,
      method: 'GET',
      data: {},
      header:{
        'Authorization': 'Bearer ' + token
      },
      success:function (res){
        console.log(res)
        that.setData({
          categories: res.data.categories
        })
      },
        fail(error){}
    })
    //热门课程
    wx.request({
      url: api.HotCourses,
      method: 'GET',
      data: {},
      header:{
        'Authorization': 'Bearer ' + token
      },
      success:function (res){
        console.log(res)
        that.setData({
          courses: res.data.courses
        })
      },
      fail(error){}
    })
  },

// 国际化
setLanguage() {
  this.setData({
    language: wx.T.getLanguage()
  });
},



  search(){
    wx.navigateTo({
      url: '../Home/search/search',
    })
  },

  HotCourses(){
    wx.navigateTo({
      url: '../Home/HotCourses/HotCourses',
    })
  },

  CareerCoachs(){
    wx.navigateTo({
      url: '../Home/CareerCoachs/CareerCoachs',
    })
  },

  OngoingCourses(){
    wx.navigateTo({
      url: './OngoingCourses/OngoingCourses',
    })
  },



})