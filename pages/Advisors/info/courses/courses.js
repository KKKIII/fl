// pages/Advisors/info/courses/courses.js
const api = require("../../../../config/api")
const app = getApp()

Page({

  /**
   * 页面的初始数据
   */
  data: {
    van_icon: 0,
    navBack: 0,
    id: 0,
    like: 0,
    hasChange: false,
    show: false,
    favor_img: "../../../../images/tabbarLogo/Heart.png",
    favor_img2: "../../../../images/tabbarLogo/like.png",
    show: true,
    value: 5,
  },

  onChange(event) {
    this.setData({
      value: event.detail,
    });
  },

  Like1(e){
    var that = this;
    var hasChange = that.data.hasChange;
    let token = app.globalData.access_token;
    if(hasChange !== undefined){
       var onum = parseInt(that.data.like);
       console.log(hasChange);
       if(hasChange == 'true'){
         that.data.like = (onum - 1);
         that.data.hasChange = 'false';
         that.data.show = false;
       }else {
         that.data.like = (onum + 1);
         that.data.hasChange = 'true';
         that.data.show = true;
       }
       this.setData({
         like: that.data.like,
         hasChange: that.data.hasChange,
         show: that.data.show
       })
    };
    wx.request({
     url: api.LikeCourse,
     method: 'POST',
     data: {
       user_id: app.globalData.id,
       course_id: wx.getStorageSync('coach_id')
     },
     header:{
       'Authorization': 'Bearer ' + token
     },
     success(res){
       console.log(res);
     },
     fail(res){
       console.log(res);
     }
   });
   var pages = getCurrentPages();
   if (pages.length > 1){
     var beforePage = pages[pages.length - 2];
     beforePage.changeData();
   }



 },


  videoPlay(){
    let that = this
    let videoplay = wx.createVideoContext('video')
    videoplay.play()
    that.setData({
      van_icon: 1,
      controls: true,
      navBack: 1
    })
  },
  videoStop(){
    let that = this
    that.setData({
      van_icon: 0,
      controls: false,
      navBack: 0
    })
  },
  back(){
    wx.navigateBack({
      delta: 0,
    })
  },
  onLoad(e){
    let that = this
    console.log(e)
    that.setData({
      course_name: e.course_name,
      course_notes: e.course_notes,
      what_you_will_learn: e.what_you_will_learn,
      what_skill_will_gain: e.what_skill_will_gain,
      course_weblink: e.course_weblink,
      videosource: e.videosource,
      id1: e.id
    })
    wx.setStorageSync('coach_id', e.id)
  },
 
  Like(){
    console.log(getApp().globalData.id)
    console.log(getApp().globalData.access_token)
    this.setData(getApp().globalData)
    var that = this
      //首页文章喜欢
      wx.request({
      url: api.LikeCourse,
      method: 'POST',
      data: {
        user_id: wx.getStorageSync('id'),
        course_id: e.id
      },
      header:{
        'Authorization': 'Bearer ' + token
      },
      success:function (res){
        console.log(res)
        wx.showToast({
          title: 'Success',
        })
        that.setData({
        })
      },
      fail(error){}
    })
   },
  

  onShareAppMessage: function () {
    return {
      title: '您的朋友邀请您观看此项课程'
    }
  },

  showreview(){
    let token = app.globalData.access_token
    this.setData(getApp().globalData)
    var that = this
    wx.request({
      url: api.ShowReview + wx.getStorageSync('coach_id'),
      method: 'GET',
      data: {},
      header:{
        'Authorization': 'Bearer ' + token
      },
      success(res){
        console.log(res)
        that.setData({
          rate: res.data.reviews
        })
      }
    })
  },

  changeprofile(e){
    let temp = {}
    temp[e.currentTarget.dataset.name] = e.detail.value
    this.setData(temp)
  },

  postreview(){
    let id = app.globalData.id
    let token = app.globalData.access_token
    let that = this
    let self = this
    wx.request({
      url: api.PostReview,
      method: 'POST',
      data: {
        course_id: wx.getStorageSync('coach_id'),
        user_id: id,
        ratings: self.data.value,
        reviews: self.data.feedback
      },
      header:{
        'Authorization': 'Bearer ' + token
      },
      success(res){
        console.log(res)
        that.setData({
          show: false
        })
      }
    })
  },

  cross(){
    let that = this
    that.setData({
      show: false
    })
  }

})