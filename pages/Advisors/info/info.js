const api = require("../../../config/api")
const { ExploreAdvisors } = require("../../../config/api")
const app = getApp()


// pages/Advisors/info/info.js
Page({

  /**
   * 页面的初始数据
   */
  data: {
    Articles_box_text1_like: "../../static/tabbarLogo/like.png",
    src: './fbc8f6fd6e5dc2d03d5f323e1f83bc34.mp4',
    van_icon: 0,
    controls: false,
    navBack: 0,
    getInfo: 0,
    category:[
      "All",
      "Personality",
      "Leadership",
      "Category",
      "All",
      "Personality",
      "Leadership",
      "Category"
    ],
    like: 0,
    hasChange: false,
    show: false,
    favor_img: "../../../images/tabbarLogo/Heart.png",
    favor_img2: "../../../images/tabbarLogo/like.png"
  },

  Like1:function(e){
    var that = this;
    var hasChange = that.data.hasChange;
    let token = app.globalData.access_token;
    if(hasChange !== undefined){
       var onum = parseInt(that.data.like);
       console.log(hasChange);
       if(hasChange == 'true'){
         that.data.like = (onum - 1);
         that.data.hasChange = 'false';
         that.data.show = false;
       }else {
         that.data.like = (onum + 1);
         that.data.hasChange = 'true';
         that.data.show = true;
       }
       this.setData({
         like: that.data.like,
         hasChange: that.data.hasChange,
         show: that.data.show
       })
    };
    wx.request({
     url: api.LikeAdvisor,
     method: 'POST',
     data: {
       user_id: wx.getStorageSync('id'),
       advisor_id: wx.getStorageSync('advisor_id')
     },
     header:{
       'Authorization': 'Bearer ' + token
     },
     success:function(res){
       console.log(res);
     },
     fail: function(res){
       console.log(res);
     }
   });
   var pages = getCurrentPages();
   if (pages.length > 1){
     var beforePage = pages[pages.length - 2];
     beforePage.changeData();
   }



 },

  videoPlay(){
    let that = this
    let videoplay = wx.createVideoContext('video')
    videoplay.play()
    that.setData({
      van_icon: 1,
      controls: true,
      navBack: 1
    })
  },
  videoStop(){
    let that = this
    that.setData({
      van_icon: 0,
      controls: false,
      navBack: 0
    })
  },
  back(){
    wx.navigateBack({
      delta: 0,
    })
  },
  onLoad(e){
    let that = this
    let token = app.globalData.access_token
    let id = app.globalData.id
    console.log(e)
    that.setData({
      id: e.id
    })
    wx.setStorageSync('advisor_id', e.id)
    wx.request({
      url: api.ViewSingleCoach + e.id,
      method: 'GET',
      data:{},
      header:{
        'Authorization': 'Bearer ' + token
      },
      success:function (res){
        console.log(res)
        if(res.data.coach){
          that.setData({
            full_name: res.data.coach.full_name,
            location: res.data.coach.location,
            skills_chosen: res.data.coach.skills_chosen,
            profile_image: res.data.coach.profile_image,
            video_link: res.data.coach.video_link,
            id: res.data.coach.id
          })
        }else if(!res.data.coach){
          wx.showToast({
            title: '顾问暂未提交完成所有资料',
            duration: 3000
          })
          wx.navigateBack({
            delta:0,
            duration:3000
          })
        }
      }
    }) ,
    wx.request({
      url: api.GetCoursesByCoach + e.id,
      method: 'GET',
      data: {},
      header:{
        'Authorization': 'Bearer ' + token
      },
      success(res){
        console.log(res)
        that.setData({
          courses: res.data.courses
        })
      }
    }),
    wx.request({
      url: api.GetArticleByCoach + e.id,
      method: 'GET',
      data: {},
      header:{
        'Authorization': 'Bearer ' + token
      },
      success(res){
        console.log(res)
  
      }
    })
 
    //聊天记录
    wx.request({
      url: api.ReceiveChat,
      method: 'POST',
      data: {
        from: id,
        to: e.id
      },
      header:{
        'Authorization': 'Bearer ' + token
      },
      success(res){
        console.log(res)
      }
    })

    //发送聊天
    wx.request({
      url: api.SendChat,
      method: 'POST',
      data:{
        from: id,
        to: e.id,
        message: self.data.message
      },
      header:{
        'Authorization': 'Bearer ' + token
      },
      success(res){
        console.log(res)
      }
    })

  },
  Like(e){
    let that = this
    console.log(e)
    that.setData({
      id: e.id
    })
    wx.request({
      url: api.LikeAdvisor,
      method: 'POST',
      data:{
        user_id: wx.getStorageSync('id'),
        advisor_id: wx.getStorageSync('advisor_id')
      },
      header:{
        'Authorization': 'Bearer ' + token
      },
      success:function (res){
        console.log(res)
        wx.showToast({
          title: 'Success',
        })
      },
      fail(error){}
    })
  },


  navCourses(e){
    console.log(e.currentTarget.dataset.id)
    wx.navigateTo({
      url: './courses/courses?id=' + e.currentTarget.dataset.id
    })
  },

  follow(){
    let id = app.globalData.id
    let token = app.globalData.access_token
    let that = this
    wx.request({
      url: api.FollowCoach + id,
      method: 'POST',
      data: {
        coach_id : wx.getStorageSync('advisor_id')
      },
      header:{
        'Authorization': 'Bearer ' + token
      },
      success(res){
        console.log(res)
        if(res.data.status === 3){
          that.setData({
            follow: res.data.status === 3
          })
          wx.showToast({
            title: '关注成功',
          })
        }else if(res.data.status === 0){
          that.setData({
            unfollow: res.data.status === 0
          })
          wx.showToast({
            title: '您已关注',
          })
        }
      }
    })
  },

  unfollow(){
    let id = app.globalData.id
    let token = app.globalData.access_token
    let that = this
    wx.request({
      url: api.UnFollowCoach + id,
      method: "POST",
      data:{
        coach_id: wx.getStorageSync('advisor_id')
      },
      header:{
        'Authorization': 'Bearer ' + token
      },
      success(res){
        console.log(res)
      }
    })
  },

  chat(options){
      let id = options.currentTarget.dataset.id
      wx.navigateTo({
        url: '../../Chat/chatWith/chatWith?id=' + id
      })
  }

})