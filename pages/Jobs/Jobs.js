// pages/Jobs/Jobs.js
const api = require('../../config/api.js')
const app = getApp()
var id = wx.getStorageSync('id')

import event from '../../utils/event'

Page({

  /**
   * 页面的初始数据
   */
  data: {
    active: 0,
    category:[
      "All",
      "Personality",
      "Leadership",
      "Category",
      "All",
      "Personality",
      "Leadership",
      "Category"
    ],
    show: false,
    overlay: 0,
    Filter:{
      sortBy:'',
      date:'',
      Industry:''
    },
    dateName:{
      "hours": 'Past 24 hours',
      "Week": 'Past Week',
      "Month": 'Past Month',
      "Any": 'Any Time'
    },
    dateNum:{
          "hours": 32,
          "Week": 116,
          "Month": 400,
          "Any": 1852
    },
    IndustryName:{
      "Computer": 'Computer Software',
      "Financial": 'Financial Services',
      "Marketing": 'Marketing & Advertising',
      "Information": 'Information Technology & Services'
    },
    IndustryNum:{
      "Computer": 30,
      "Financial": 25,
      "Marketing": 27,
      "Information": 158
    },
    test:'11',
    language: ''
  },
  select(options){
    let id = options.currentTarget.dataset.id
    wx.navigateTo({
      url: '../Home/JobCategoryDetail/JobCategoryDetail?id=' + id,
    })
  },
  navDes(options){
    let id = options.currentTarget.dataset.id
    wx.navigateTo({
      url: './Description/Description?id=' + id
    })
  },
  onClickShow(){
    let that = this
    that.setData({ overlay: 1 });
  },
  close(){
    let that = this
    that.setData({ overlay: 0 });
    console.log('隐藏')
  },
  SortClick(event) {
    const { name } = event.currentTarget.dataset;
    let sortBy = 'Filter.sortBy'
    this.setData({
      [sortBy]: name,
    });
    console.log("Sort by",this.data.Filter.sortBy)
  },
  DateClick(event){
    const { name } = event.currentTarget.dataset;
    let date = 'Filter.date'
    this.setData({
      [date]: name,
    });
    console.log("Sort by",this.data.Filter.date)
  },
  IndustryClick(event){
    const { name } = event.currentTarget.dataset;
    let Industry = 'Filter.Industry'
    this.setData({
      [Industry]: name,
    });
    console.log("Sort by",this.data.Filter.Industry)
  },
  done(){
    let that = this
    that.setData({ overlay: 0 });
    console.log(this.data.Filter)
  },
  navHome(){
    wx.switchTab({
      url: '../Home/Home',
    })
  },
  onShareAppMessage(){
    return{
      image: '../static/lgoo.png',
      title: 'Future Leaders',
      path: '../Start/Start'
    }
  },
  share(){
    wx.showShareMenu({
      menus: [],
    })
  },
  onShareTimeline(res) {
    return {
      imageUrl:'../static/tabbarLogo/lgoo.png',
      title: 'Futrue Leaders',
      path:'../Start/Start'
    }
  },
  test(){
    wx.navigateTo({
      url: '../Home/search/search'
    })
  },
  onLoad(){
    console.log(getApp().globalData.id)
    console.log(getApp().globalData.access_token)
    this.setData(getApp().globalData)
    var that = this
    let token = app.globalData.access_token
    let id = app.globalData.id
            // 国际化
this.setLanguage();  // (1)
event.on("languageChanged", this, this.setLanguage); // (2)
    //工作页分类
    wx.request({
      url: api.GetJobCategories,
      method: 'GET',
      data: {
      },
      header:{
        'Authorization': 'Bearer ' + token
      },
      success:function (res){
        console.log(res)
        that.setData({
          categories: res.data.categories
        })
      },
      fail(error){}
    })
    wx.request({
      url: 'https://futureleaderstech.com/api/jobs/aroundtheworld/100/100',
      method: 'GET',
      data:{},
      header:{
        'Authorization': 'Bearer ' + token
      },
      success:function (res){
        console.log(res)
        that.setData({
          job: res.data.jobs
        })
      },
      fail(error){}
    })
    
    //工作页工作提醒
    wx.request({
      url: 'https://futureleaderstech.com/api/user/jobsalertbased/' + id,
      method: 'GET',
      data:{},
      header:{
        'Authorization': 'Bearer ' + token
      },
      success:function (res){
        console.log(res)
        that.setData({

        })
      },
      fail(error){}
    })



  },

  // 国际化
setLanguage() {
  this.setData({
    language: wx.T.getLanguage()
  });
},

})