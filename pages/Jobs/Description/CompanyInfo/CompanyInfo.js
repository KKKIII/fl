// pages/Advisors/info/info.js
Page({

  /**
   * 页面的初始数据
   */
  data: {
    Articles_box_text1_like: "../../static/tabbarLogo/like.png",
    src: './fbc8f6fd6e5dc2d03d5f323e1f83bc34.mp4',
    van_icon: 0,
    controls: false,
    navBack: 0,
    getInfo: 0,
    category:[
      "All",
      "Personality",
      "Leadership",
      "Category",
      "All",
      "Personality",
      "Leadership",
      "Category"
    ]
  },
  videoPlay(){
    let that = this
    let videoplay = wx.createVideoContext('video')
    videoplay.play()
    that.setData({
      van_icon: 1,
      controls: true,
      navBack: 1
    })
  },
  videoStop(){
    let that = this
    that.setData({
      van_icon: 0,
      controls: false,
      navBack: 0
    })
  },
  back(){
    wx.navigateBack({
      delta: 0,
    })
  },
  onLoad(e){
    let that = this
    console.log(e)
    that.setData({
      getInfo: e.info
    })
  },
  navCourses(e){
    console.log(e.currentTarget.dataset.id)
    wx.navigateTo({
      url: './courses/courses?id=' + e.currentTarget.dataset.id
    })
  }
})