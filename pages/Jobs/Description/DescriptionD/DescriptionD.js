// pages/Jobs/Description/DescriptionD/DescriptionD.js
const api = require('../../../../config/api.js')
const app = getApp()

Page({

  /**
   * 页面的初始数据
   */
  data: {
    reShow: false,
    applyShow: false
  },

  /**
   * 生命周期函数--监听页面加载
   */
  onLoad () {
    let that = this
    let token = app.globalData.access_token
    wx.request({
      url: api.GetJobsById + wx.getStorageSync('jobs_id'),
      method: 'GET',
      data:{},
      header:{
        'Authorization': 'Bearer' + token
      },
      success:function(res){
        console.log(res)
        that.setData({
          job_description: res.data.job.job_description,
          employer_name: res.data.job.employer_name,
          category_description: res.data.job.category_description,
          category_icon: res.data.job.category_icon,
          hiring_manager: res.data.job.hiring_manager
        })
      }

    })
  },

  onClickShow() {
    this.setData({ applyShow: true });
  },

  navApply(){
    wx.navigateTo({
      url: '../Application/Application'
    })
  },
  /**
   * 生命周期函数--监听页面初次渲染完成
   */
  onReady: function () {

  },

  /**
   * 生命周期函数--监听页面显示
   */
  onShow: function () {

  },

  /**
   * 生命周期函数--监听页面隐藏
   */
  onHide: function () {

  },

  /**
   * 生命周期函数--监听页面卸载
   */
  onUnload: function () {

  },

  /**
   * 页面相关事件处理函数--监听用户下拉动作
   */
  onPullDownRefresh: function () {

  },

  /**
   * 页面上拉触底事件的处理函数
   */
  onReachBottom: function () {

  },

  /**
   * 用户点击右上角分享
   */
  onShareAppMessage: function () {

  }
})