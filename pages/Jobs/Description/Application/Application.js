// pages/Jobs/Description/Application/Application.js
const utils = require('../../../../utils/util.js')
const api = require('../../../../config/api.js')
const app = getApp()

Page({

  /**
   * 页面的初始数据
   */
  data: {
    Document_text:[],
    Document_video:[]
  },
  navBack(){
    wx.navigateBack({
      delta: 0
    })
  },
  submit(e){
    console.log(e.detail.value)
    let id = app.globalData.id
    let token = app.globalData.access_token
    wx.request({
      url: api.ApplyJobs + id,
      method: 'POST',
      data:{
        user_id: id,
        job_id: wx.getStorageSync('jobs_id'),
        cover_letter: e.detail.value,
        show_assessment: 'yes',
        show_courses_attended: 'YES'
      },
      header:{
        'Authorization': 'Bearer ' + token,
        "Content-Type": "application/x-www-form-urlencoded"
      },
      success:function(res){
        console.log(res)
        wx.showModal({
          title: 'Applied',
          content: 'Lorem ipsum dolor sit amet, consectetur adipiscing elit, sed do eiusmod tempor',
          success: function (res) {
            if (res.cancel) {
               //点击取消,默认隐藏弹框
            } else {
               wx.switchTab({
                 url: '../../Jobs',
               })
            }
         },

        })
      }   
    })
  },
  upText(){
    console.log('1')
    let that = this
    wx.chooseMessageFile({
      count: 1,
      type: 'file',
      success(res){
        console.log(res.tempFiles[0])
        var name = res.tempFiles[0].name
        var time = utils.js_date_time(res.tempFiles[0].time)
        var form = [name,time]
        if(name.indexOf('.docx') == -1){
          wx.showToast({
            title: 'word',
            icon: "none"
          })
        }else{
          that.setData({Document_text: form})
          console.log(form)
        }
      }
    })
  },
  upVideo(){
    let that = this
    wx.chooseMessageFile({
      count: 1,
      type: 'video',
      success(res){
        console.log(res.tempFiles[0])
        var name = res.tempFiles[0].name
        var time = utils.formatTime(Math.round(new Date().getTime()))
        var form = [name,time]
        if(name.indexOf('.mp4') == -1){
          wx.showToast({
            title: 'mp4',
            icon: "none"
          })
        }else{
          that.setData({Document_video: form})
        }
      }
    })
  },
  delText(){
    this.setData({Document_text: []})
  },
  delVideo(){
    this.setData({Document_video: []})
    console.log('1')
  },
  removeAll(){
    this.setData({Document_text: []})
    this.setData({Document_video: []})
  },
   
  onLoad(){
    let that = this
    let token = app.globalData.access_token
    let jobs_id = wx.getStorageSync('jobs_id')
    wx.request({
      url: api.GetJobsById + jobs_id,
      method: 'GET',
      data:{},
      header:{
        'Authorization': 'Bearer' + token
      },
      success:function(res){
        console.log(res)
        that.setData({
           job_description: res.data.job.job_description,
           employer_name: res.data.job.employer_name
        })
      }

    })
  }

})