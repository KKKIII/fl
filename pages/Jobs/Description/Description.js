// pages/Jobs/Description/Description.js
const api = require('../../../config/api.js')
const app = getApp()

Page({

  /**
   * 页面的初始数据
   */
  data: {
    applyShow: false,
    reShow: false,
    upShow: false,
    Applied: false,
    ResumeName: 'Upload All'
  },
  navBack(){
    wx.navigateBack({
      delta: 0
    })
  },
  onLoad(e){
    let that = this
    let token = app.globalData.access_token
    console.log(e)
    that.setData({
      id: e.id
    })
    wx.setStorageSync('jobs_id', e.id)
    wx.request({
      url: api.GetJobsById + e.id,
      method: 'GET',
      data:{},
      header:{
        'Authorization': 'Bearer' + token
      },
      success:function(res){
        console.log(res)
        that.setData({
          job_description: res.data.job.job_description,
          employer_name: res.data.job.employer_name,
          category_description: res.data.job.category_description,
          category_icon: res.data.job.category_icon
        })
      }

    })
  },
  test(){
    wx.navigateTo({
      url: './CompanyInfo/CompanyInfo'
    })
  },
  Apply(){
    
  },
  onClickShow() {
    this.setData({ applyShow: true });
  },
  onRe() {
    wx.navigateTo({
      url: './DescriptionD/DescriptionD',
    })
  },
  onUp(event){
    console.log(event.currentTarget.dataset)
    this.setData({ResumeName: event.currentTarget.dataset.select,reShow: false,upShow: true})
  },
  notNow(){
    this.setData({upShow: false, applyShow: false,Applied: false});
  },
  Applied(){
    this.setData({upShow: false, Applied: true});
  },
  navApply(){
    wx.navigateTo({
      url: './Application/Application'
    })
  }
})