// pages/Chat/Chat.js
import event from '../../utils/event'


Page({

  /**
   * 页面的初始数据
   */
  data: {
    list: '',
    language: ''
  },
  onLoad(){
    var self = this
                // 国际化
this.setLanguage();  // (1)
event.on("languageChanged", this, this.setLanguage); // (2)
    wx.request({
      url: 'http://mock-api.com/wz2r5DzL.mock/chat',
      success(res){
        console.log(res.data)
        self.setData({list: res.data})
      }
    })
  },

  // 国际化
  setLanguage() {
    this.setData({
      language: wx.T.getLanguage()
    });
  },

  navChat(event){
    wx.navigateTo({
      url: './chatWith/chatWith?id=' + event.currentTarget.dataset.id + '&img=' + event.currentTarget.dataset.img
    })
  },
  navHome(){
    wx.switchTab({
      url: '../Home/Home',
    })
  },

  goNo(){
    wx.navigateTo({
      url: '../Home/Notifications/Notifications',
    })
  }

})