// pages/Chat/chatWith/chatWith.js
const api = require("../../../config/api")
const app = getApp()

Page({

  /**
   * 页面的初始数据
   */
  data: {
    id: '',
    img: '',
    path: '',
    sendStatus: 0,
    imgStatus: 0
  },
  navBack(){
    wx.navigateBack({
      delta: 0
    })
  },
  onLoad(e){
    let that = this
    let token = app.globalData.access_token
    let id = app.globalData.id
    console.log(e)
    that.setData({
      id: e.id,
      profile_image: e.profile_image,
      full_name: e.full_name
    })

        //聊天记录
        wx.request({
          url: api.ReceiveChat,
          method: 'POST',
          data: {
            from: id,
            to: e.id
          },
          header:{
            'Authorization': 'Bearer ' + token
          },
          success(res){
            console.log(res)
            that.setData({
              messages: res.data.messages
            })
          }
        })
    
        //发送聊天
        wx.request({
          url: api.SendChat,
          method: 'POST',
          data:{
            from: id,
            to: e.id,
            message: self.data.message
          },
          header:{
            'Authorization': 'Bearer ' + token
          },
          success(res){
            console.log(res)
          }
        })
  },
  say(res){
    var self = this
    wx.startRecord({
      success(res){
        self.setData({path: res.tempFilePath})
      }
    })
  },
  stop(){
    wx.stopRecord()
    console.log(this.data.path)
    
  },
  listen(){
    var self = this
    wx.playVoice({
      filePath: self.data.path,
    })
  },
  //获取输入内容
  getText(e){
    this.setData({value: e.detail.value})
    if(!e.detail.value){
      this.setData({imgStatus: 1})
    }else{
      this.setData({imgStatus: 0})
    }
  },
  //语音与文本输入框转换
  Vary(){
    if(this.data.sendStatus == 1){
      this.setData({sendStatus: 0})
    }else{
      this.setData({sendStatus: 1})
    }
  },
  // 发送信息
  sendMes(){
    if(!this.data.value){
      return
    }else{
      console.log(this.data.value)
    }
  }
})