// pages/Home/HotCourses/HotCourses.js
const api = require('../../../config/api.js')
const app = getApp()
var id = wx.getStorageSync('id')

Page({

  /**
   * 页面的初始数据
   */
  data: {
    job:[
      "111","111","111","111","111","111"
    ],
    Suggested_Articles:[
      "1",
      "1"
    ],
    Articles_box_text1_like: "../static/tabbarLogo/like.png",
    img:"https://pic2.zhimg.com/50/v2-a9e22fe9fea034fd97f129dc18008816_r.jpg",
    showLength:5
  },
  test(){
    wx.navigateTo({
      url: './search/search'
    })
  },
  onLoad(){
    console.log(getApp().globalData.id)
    console.log(getApp().globalData.access_token)
    this.setData(getApp().globalData)
    var that = this
    let token = app.globalData.access_token
    //首页热门课程
    wx.request({
      url: api.HotCourses,
      method: 'GET',
      data: {},
      header:{
        'Authorization': 'Bearer ' + token
      },
      success:function (res){
        console.log(res)
        that.setData({
          courses: res.data.courses
        })
      },
      fail(error){}
    })

  },



})