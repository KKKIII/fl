// pages/Home/search/search.js
const api = require('../../../config/api.js')
var token = wx.getStorageSync('token')

Page({

  /**
   * 页面的初始数据
   */
  data: {
    recordsList: [],   //用来显示记录的循环数组
    value: ''  //输入的内容
  },
  searchChange(event){
    this.setData({value: event.detail})
  },
  back(){
    wx.navigateBack({
      delta: 0,
    })
  },
  // 打开页面显示搜索记录
  onShow(){
    let self = this
    // 获取本地缓存
    wx.getStorage({
      key: 'search',
      success(res){
        console.log(res.data)
        self.setData({recordsList: res.data})
      }
    })
  },
  // 回车键触发搜索
  goSearch(){
    var arr = this.data.recordsList
    arr.unshift(this.data.value)
    this.setData({recordsList: arr}) 
    var that = this
    wx.navigateTo({
      url: '../SearchDetail/SearchDetail?search_word=' + this.data.value 
    })
    //存储本地缓存
    wx.setStorage({
      key:"search",
      data: arr,
      success(){
        console.log(arr)
      }
    })
  },
  // 删除搜素记录
  delRec(res){
    let id = res.currentTarget.dataset.id
    let list = this.data.recordsList
    list.splice(id, 1)     //删除单条
    this.setData({recordsList: list})
    //存储删除后的数组
    wx.setStorage({
      key:"search",
      data: list
    })
  }
})