// pages/Home/CareerCoachs/CareerCoachs.js
const api = require('../../../config/api.js')
const app = getApp()

Page({

  /**
   * 页面的初始数据
   */
  data: {
    active: 0,
    category:[
      "All",
      "Personality",
      "Leadership",
      "Category",
      "All",
      "Personality",
      "Leadership",
      "Category"
    ],
    Advisors: [""],
    showLength:5
  },
  select(e){
    let that = this
    console.log(e.currentTarget.dataset.id)
    that.setData({
      active: e.currentTarget.dataset.id
    })
  },
  navMes(options){
    let id = options.currentTarget.dataset.id
    wx.navigateTo({
      url: '../../Advisors/info/info?id=' + id
    })
  },
  navHome(){
    wx.switchTab({
      url: '../Home/Home',
    })
  },
  onShareAppMessage(){
    return{
      imageUrl: '../static/lgoo.png',
      title: 'Future Leaders',
      path: '../Start/Start'
    }
  },
  share(){
    wx.showShareMenu({
      menus: [],
    })
  },
  onShareTimeline(res) {
    return {
      imageUrl:'../static/tabbarLogo/lgoo.png',
      title: 'Futrue Leaders',
      path:'../Start/Start'
    }
  },
  onLoad(){
    console.log(getApp().globalData.id)
    console.log(getApp().globalData.access_token)
    this.setData(getApp().globalData)
    var that = this
    let token = app.globalData.access_token
    //顾问
    wx.request({
      url: api.ExploreAdvisors,
      method: 'GET',
      data: {},
      header:{
        'Authorization': 'Bearer ' + token
      },
      success:function (res){
        console.log(res)
        wx.setStorageSync('coach_id', res.data.advisors[0].id)
        that.setData({
          advisors: res.data.advisors
        })
      },
      fail(error){}
    })
  },

  search(){
    wx.navigateTo({
      url: '../Home/search/search',
    })
  },

  HotCourses(){
    wx.navigateTo({
      url: '../Home/HotCourses/HotCourses',
    })
  },
})