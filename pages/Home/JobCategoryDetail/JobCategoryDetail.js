const api = require("../../../config/api")
const app = getApp()

// pages/Home/JobCategoryDetail/JobCategoryDetail.js
Page({

  /**
   * 页面的初始数据
   */
  data: {
    id: 0,
    job:''
  },

  /**
   * 生命周期函数--监听页面加载
   */
  onLoad(e){
    let that = this
    let token = app.globalData.access_token
    console.log(e)
    that.setData({
      id: e.id  ,
      employer_name: e.category_name
    })
    wx.request({
      url: api.ByCategoryJobs + e.id,
      method: 'GET',
      data:{},
      header:{
        'Authorization': 'Bearer' + token
      },
      success:function(res){
        console.log(res)
        that.setData({
          gg: res.data.jobs
        })
      }

    })
 
  },


  navDes(options){
    let id = options.currentTarget.dataset.id
    wx.navigateTo({
      url: '../../Jobs/Description/Description?id=' + id
    })
  },

  /**
   * 生命周期函数--监听页面初次渲染完成
   */
  onReady: function () {

  },

  /**
   * 生命周期函数--监听页面显示
   */
  onShow: function () {

  },

  /**
   * 生命周期函数--监听页面隐藏
   */
  onHide: function () {

  },

  /**
   * 生命周期函数--监听页面卸载
   */
  onUnload: function () {

  },

  /**
   * 页面相关事件处理函数--监听用户下拉动作
   */
  onPullDownRefresh: function () {

  },

  /**
   * 页面上拉触底事件的处理函数
   */
  onReachBottom: function () {

  },

  /**
   * 用户点击右上角分享
   */
  onShareAppMessage: function () {

  }
})