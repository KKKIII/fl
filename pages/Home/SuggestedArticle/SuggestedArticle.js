// pages/Home/SuggestedArticle/SuggestedArticle.js
const api = require('../../../config/api.js')
const app = getApp()
var id = wx.getStorageSync('id')
var article_id = wx.getStorageSync('article_id')

Page({

  /**
   * 页面的初始数据
   */
  data: {
    job:[
      "111","111","111","111","111","111"
    ],
    Suggested_Articles:[
      "1",
      "1"
    ],
    Articles_box_text1_like: "../static/tabbarLogo/like.png",
    img:"https://pic2.zhimg.com/50/v2-a9e22fe9fea034fd97f129dc18008816_r.jpg",
    showLength:5,
    favor_img: "../../../images/tabbarLogo/Heart.png",
    favor_img2: "../../../images/tabbarLogo/like.png"
  },
  test(){
    wx.navigateTo({
      url: './search/search'
    })
  },
  onLoad(){
    console.log(getApp().globalData.id)
    console.log(getApp().globalData.access_token)
    this.setData(getApp().globalData)
    var that = this
    let token = app.globalData.access_token
    //首页工作分类
    wx.request({
      url: api.GetJobCategories,
      method: 'GET',
      data: {},
      header:{
        'Authorization': 'Bearer ' + token
      },
      success:function (res){
        console.log(res)
        that.setData({
          categories: res.data.categories
        })
      },
      fail(error){}
    })
    //首页推荐文章
    wx.request({
      url: api.SuggestedArticles,
      method: 'GET',
      data: {},
      header:{
        'Authorization': 'Bearer ' + token
      },
      success:function (res){
        console.log(res)
        that.setData({
          articles: res.data.articles
        })
      },
      fail(error){}
    })
    //首页顾问
    wx.request({
      url: api.ExploreAdvisors,
      method: 'GET',
      data: {},
      header:{
        'Authorization': 'Bearer ' + token
      },
      success:function (res){
        console.log(res)
        wx.setStorageSync('coach_id', res.data.advisors[0].id)
        that.setData({
          advisors: res.data.advisors
        })
      },
      fail(error){}
    })
    //首页热门课程
    wx.request({
      url: api.HotCourses,
      method: 'GET',
      data: {},
      header:{
        'Authorization': 'Bearer ' + token
      },
      success:function (res){
        console.log(res)
        that.setData({
          courses: res.data.courses
        })
      },
      fail(error){}
    })

  },
 JobCategory(){
   wx.navigateTo({
     url: './JobCategory/JobCategory',
   })
 },
SuggestedArticles(){
  wx.navigateTo({
    url: './SuggestedArticle/SuggestedArticle',
  })
},
HotCourses(){
  wx.navigateTo({
    url: './HotCourses/HotCourses',
  })
},
HotCourses1(e){
  console.log(e.currentTarget.dataset.id)
  let self = this
  wx.navigateTo({
    url: `../Advisors/info/courses/courses?id=${self.data.courses.course_name}`
  })
},
 Like(){
  console.log(getApp().globalData.id)
  console.log(getApp().globalData.access_token)
  this.setData(getApp().globalData)
  var that = this
    //首页文章喜欢
    wx.request({
    url: api.Likearticle,
    method: 'POST',
    data: {
      user_id: wx.getStorageSync('id'),
      article_id: wx.getStorageSync('article_id')
    },
    header:{
      'Authorization': 'Bearer ' + token
    },
    success:function (res){
      console.log(res)
      wx.showToast({
        title: 'Success',
      })
      that.setData({
      })
    },
    fail(error){}
  })
 },
 CareerCoachs(){
   wx.navigateTo({
     url: './CareerCoachs/CareerCoachs',
   })
 },

 goarticle(options){
  let id = options.currentTarget.dataset.id
  var that = this
  wx.navigateTo({
    url: '../ArticleDetail/ArticleDetail?id=' + id
  })
 },
})