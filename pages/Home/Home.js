// pages/Home/Home.js
const api = require('../../config/api.js')
const app = getApp()


import event from '../../utils/event'

Page({

  /**
   * 页面的初始数据
   */
  data: {
    job:[
      "111","111","111","111","111","111"
    ],
    Suggested_Articles:[
      "1",
      "1"
    ],
    Articles_box_text1_like: "../static/tabbarLogo/like.png",
    img:"https://pic2.zhimg.com/50/v2-a9e22fe9fea034fd97f129dc18008816_r.jpg",
    showLength:5,
    categories:[{
      id: "1",
      category_name: "Software",
      category_icon: "../../images/tabbarLogo/Account1.png"
    },{
      id: "2",
      category_name: "IT",
      category_icon: "../../images/tabbarLogo/Applicant.png"
    },{
      id: "3",
      category_name: "Sales",
      category_icon: "../../images/tabbarLogo/Group 19994.png"
    }],
    articles:[{
      id: "1",
      postimage: "../../images/tabbarLogo/Group 19994.png",
      name: "wang",
      likes: "15"
    }],
    advisors:[{
      id: "1",
      lastname: "wang",
      fitstname: "qi",
      email: "wang@gmail.com",
      profile_image: "../../images/tabbarLogo/Group 19994.png"
    }],
    courses:[{
      id: "1",
      course_name: "Java",
      course_industry: "Software",
      created_at: "2021年",
      course_notes:"java学习课程",
      what_you_will_learn: "java的一些东西",
      what_skill_will_gain: "JAVA"
    }],
    language: '',
    like: 0,
    hasChange: false,
    show: false,
    favor_img: "../../images/tabbarLogo/Heart.png",
    favor_img2: "../../images/tabbarLogo/like.png"
  },

  Like1:function(e){
     var that = this;
     var hasChange = that.data.hasChange;
     let token = app.globalData.access_token;
     if(hasChange !== undefined){
        var onum = parseInt(that.data.like);
        console.log(hasChange);
        if(hasChange == 'true'){
          that.data.like = (onum - 1);
          that.data.hasChange = 'false';
          that.data.show = false;
        }else {
          that.data.like = (onum + 1);
          that.data.hasChange = 'true';
          that.data.show = true;
        }
        this.setData({
          like: that.data.like,
          hasChange: that.data.hasChange,
          show: that.data.show
        })
     };
     wx.request({
      url: api.Likearticle,
      method: 'POST',
      data: {
        user_id: wx.getStorageSync('id'),
        article_id: 8
      },
      header:{
        'Authorization': 'Bearer ' + token
      },
      success:function(res){
        console.log(res);
      },
      fail: function(res){
        console.log(res);
      }
    });
    var pages = getCurrentPages();
    if (pages.length > 1){
      var beforePage = pages[pages.length - 2];
      beforePage.changeData();
    }



  },

  test(){
    wx.navigateTo({
      url: './search/search'
    })
  },
  onLoad(){
    console.log(getApp().globalData.id)
    console.log(getApp().globalData.access_token)
    this.setData(getApp().globalData)
    var that = this
    let token = app.globalData.access_token
    let id = app.globalData.id
    // 国际化
this.setLanguage();  // (1)
event.on("languageChanged", this, this.setLanguage); // (2)
    //首页工作分类
    wx.request({
      url: api.GetJobCategories,
      method: 'GET',
      data: {},
      header:{
        'Authorization': 'Bearer ' + token
      },
      success:function (res){
        console.log(res)
        that.setData({
          categories: res.data.categories
        })
      },
      fail(error){}
    })
    //首页推荐文章
    wx.request({
      url: api.SuggestedArticles,
      method: 'GET',
      data: {},
      header:{
        'Authorization': 'Bearer ' + token
      },
      success:function (res){
        console.log(res)
        that.setData({
          articles: res.data.articles
        })
      },
      fail(error){}
    })
    //首页顾问
    wx.request({
      url: api.ExploreAdvisors,
      method: 'GET',
      data: {},
      header:{
        'Authorization': 'Bearer ' + token
      },
      success:function (res){
        console.log(res)
        wx.setStorageSync('article_id', res.data.id)
        that.setData({
          advisors: res.data.advisors
        })
      },
      fail(error){}
    })
    //首页热门课程
    wx.request({
      url: api.HotCourses,
      method: 'GET',
      data: {},
      header:{
        'Authorization': 'Bearer ' + token
      },
      success:function (res){
        console.log(res)
        that.setData({
          courses: res.data.courses
        })
      },
      fail(error){}
    })



    wx.request({
      url: api.GetUserCompletedProfile + id,
      method: 'GET',
      data: {},
      header:{
        'Authorization': 'Bearer ' + token
      },
      success:function (res){
        console.log(res)
        if(res.data.profile){
        that.setData({
          profile: res.data.profile
        })
        wx.request({
          url: 'https://futureleaderstech.com/api/user/recommendedjobs/' +id +'/0',      
          method: 'GET',
          data: {},
          header:{
            'Authorization': 'Bearer ' + token
          },
          success:function (res){
            console.log(res)
            that.setData({
              all: res.data.jobs
            })
          },
          fail(error){}
        })
       }else if (!res.data.profile){
         wx.showToast({
           title: '您暂未完成个人资料',
           icon: 'error'
         })

       }
      }
         
  
    })

  },


// 国际化
setLanguage() {
  this.setData({
    language: wx.T.getLanguage()
  });
},



 JobCategory(){
   wx.navigateTo({
     url: './JobCategory/JobCategory',
   })
 },
SuggestedArticles(){
  wx.navigateTo({
    url: './SuggestedArticle/SuggestedArticle',
  })
},

advisorsdetail(options){
  let id = options.currentTarget.dataset.id
  const app = getApp()
  let email = app.globalData.email
  console.log(email)
  if(email == '' ){
    wx.showToast({
      title: '您暂未登录',
      icon: 'error',
      success(res){
        wx.reLaunch({
          url: '../Start/Start',
        })
      }
    })
  }else{    
      wx.navigateTo({
        url: '../Advisors/info/info&id=' + id,
      })
  }


},
HotCoursesDetail(options){
  let id = options.currentTarget.dataset.id
  const app = getApp()
  let email = app.globalData.email
  console.log(email)
  if(email == '' ){
    wx.showToast({
      title: '您暂未登录',
      icon: 'error',
      success(res){
        wx.reLaunch({
          url: '../Start/Start',
        })
      }
    })
  }else{    
      wx.navigateTo({
        url: '../Advisors/info/info' + id,
      })
  }
},

HotCourses(){
  wx.navigateTo({
    url: './HotCourses/HotCourses',
  })
},
HotCourses1(){
    wx.navigateTo({
      url: `../Advisors/info/courses/courses?id={{item.id}}`,
      success: function(res) {
        console.log(res);
    },
    })


},
 Like(){
  let token = app.globalData.access_token
  var that = this
    //首页文章喜欢
    wx.request({
    url: api.Likearticle,
    method: 'POST',
    data: {
      user_id: wx.getStorageSync('id'),
      article_id: 8
    },
    header:{
      'Authorization': 'Bearer ' + token
    },
    success:function (res){
      console.log(res)
      if(res.data.message === "Article Liked Successfully"){
        this.setData({
          Like: true
        })
      }else if(res.data.message === "Article Like Reverted"){
        this.setData({
          Like: false
        })
      }
    },
    fail(error){}
  })
 },
 CareerCoachs(){
   wx.navigateTo({
     url: './CareerCoachs/CareerCoachs',
   })
 },

 godetail(options){
  let id = options.currentTarget.dataset.id
  const app = getApp()
  let email = app.globalData.email
  console.log(email)
  if(email == '' ){
    wx.showToast({
      title: '您暂未登录',
      icon: 'error',
      success(res){
        wx.reLaunch({
          url: '../Start/Start',
        })
      }
    })
  }else{    
      wx.navigateTo({
        url: './JobCategoryDetail/JobCategoryDetail?id=' + id,
      })
  }
 },

 goarticle(options){
  let id = options.currentTarget.dataset.id
  var that = this
  wx.navigateTo({
    url: './ArticleDetail/ArticleDetail?id=' + id
  })
 },

 gojob(options){
  let id = options.currentTarget.dataset.id
  var that = this
  wx.navigateTo({
    url: '../Jobs/Description/Description?id=' + id
  })
 },

 goprofile(){
   if(app.globalData.email){
     wx.navigateTo({
       url: '../Start/Login/Profile/Profile',
     })
   }else if(!app.globalData.email){
     wx.showToast({
       title: '请您登录后再次尝试完成资料',
     })
     wx.reLaunch({
       url: '../Start/Start',
     })
   }
 },

 onShareAppMessage: function (res) {
  return{
    title: 'Future Leaders'
  }
 },

 onShareTimeline:function(){
  return{
    imageUrl: '../../images/tabbarLogo/lgoo.png',
    title: 'Future Leaders'
  }
 },

 goNo(){
   wx.navigateTo({
     url: './Notifications/Notifications',
   })
 },

 changelanguage(){
   wx.setStorageSync('langIndex', 1)
 }


})

