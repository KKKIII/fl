// app.js
const api = require('./config/api.js')

import locales from './utils/locales'
import T from './utils/i18n'
import event from './utils/event'

T.registerLocale(locales);  // (1)
T.setLocaleByIndex(wx.getStorageSync('langIndex') || 0); // (2)
wx.T = T;  // (3)

App({
  onLaunch:function() {
    //查询登录状态
    // const login = wx.getStorageSync('login')
    // console.log(login)
    // if(!login){
    //   console.log('未登录')
    // }else {
    //   wx.reLaunch({
    //     url: '/pages/Home/Home',
    //   })
    // }
    wx.getSystemInfo({
      success(res){

// 判断系统语言 确定小程序语言
if ( res.language === 'zh_CN') {
  wx.T.setLocaleByIndex(0);
  event.emit('languageChanged');
  wx.setStorage({
    key: 'langIndex',
    data: 0
  })
}else{
  wx.T.setLocaleByIndex(1);
  event.emit('languageChanged');
  wx.setStorage({
    key: 'langIndex',
    data: 1
  })
}

      }
    })

  },
  globalData: {
    role:'',
    userInfo: null,
    id: '',
    email: '',
    password: '',
    firstname: '',
    lastname: '',
    user_role: '',
    access_token: '',
    phone_number: '',
    profile_image: '',
    full_name:''
  },
})