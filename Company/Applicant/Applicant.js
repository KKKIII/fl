// Company/Applicant/Applicant.js
Page({

  /**
   * 页面的初始数据
   */
  data: {
    current: 0
  },
  navHome(){
    wx.navigateTo({
      url: '../Home/Home',
    })
  },
  navTabbar(e){
    console.log(e.currentTarget.dataset.url)
    wx.navigateTo({
      url: `../${e.currentTarget.dataset.url}/${e.currentTarget.dataset.url}`,
    })
  },
  changeTabs(e){
    this.setData({
      current: e.currentTarget.dataset.id
    })
  },
  navDe(){
    wx.navigateTo({
      url: './Detail/Detail',
    })
  },
  navDeDel(){
    wx.navigateTo({
      url: `./Detail/Detail?judgment=${1}`,
    })
  },
  changeCurrent(event){
    this.setData({
      current: event.detail.current
    })
  },
  onPullDownRefresh(){
    this.onShow()
    setTimeout(() => {
      wx.stopPullDownRefresh({
        success: (res) => {
          console.log('1')
        }
      })
    }, 300);
  }
})