// Company/Applicant/Detail/Detail.js
Page({

  /**
   * 页面的初始数据
   */
  data: {
    value: ''
  },
  onLoad(option){
    this.setData(option)
  },
  get(e){
    console.log(e.detail.value)
    this.setData({
      value: e.detail.value.test
    })
  },
  navBack(){
    wx.navigateBack({
      delta: 0
    })
  }
})