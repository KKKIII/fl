// Advisor/Course/Post/Post.js
Page({

  /**
   * 页面的初始数据
   */
  data: {
    current: 0,
    videoPath: '',
    videoNmae: 'Upload Video',
    LocationTitle: '1hours',
    LocationNames: '',
    LocationList: [
      '1hours',
      '2hours',
      '3hours',
      '4hours'
    ],
    SalaryNames: '',
    SalaryTitle: '1Salary',
    SalaryList: [
      '1Salary',
      '2Salary',
      '3Salary',
      '4Salary'
    ],
    EmploymentNames: '',
    EmploymentTitle: 'select',
    EmploymentList: [
      'English',
      'Chinese',
      'French',
      'German'
    ],
    LevelNames: '',
    LevelTitle: 'select',
    LevelList: [
      '1',
      '2',
      '3',
      '4'
    ],
    ManagerNames: '',
    ManagerTitle: 'select',
    ManagerList: [
      '5',
      '6',
      '7',
      '8'
    ],
    skill: [],
    switch1: false,
    switch1: false,
    switch1: false,
    switch1: false
  },
  navBack() {
    wx.navigateBack({
      delta: 0
    })
  },
  currentBack() {
    var self = this
    var num = this.data.current
    if (num > 0) {
      num -= 1
      self.setData({
        current: num
      })
    }
  },
  forbid() {},
  next() {
    if (this.data.current < 2) {
      let num = this.data.current
      this.setData({
        current: num + 1
      })
    } else console.log('3')
  },
  onChange(event) {
    console.log(event.currentTarget.dataset.name)
    let name = event.currentTarget.dataset.name
    this.setData({
      [name]: event.detail,
    });
  },
  LocationSel(event) {
    console.log(event.currentTarget.dataset.sel)
    this.setData({
      LocationNames: [''],
      LocationTitle: event.currentTarget.dataset.sel
    });
  },
  SalarySel(event) {
    console.log(event.currentTarget.dataset.sel)
    this.setData({
      SalaryNames: [''],
      SalaryTitle: event.currentTarget.dataset.sel
    });
  },
  EmploymentSel(event) {
    console.log(event.currentTarget.dataset.sel)
    this.setData({
      EmploymentNames: [''],
      EmploymentTitle: event.currentTarget.dataset.sel
    });
  },
  LevelSel(event) {
    console.log(event.currentTarget.dataset.sel)
    this.setData({
      LevelNames: [''],
      LevelTitle: event.currentTarget.dataset.sel
    });
  },
  ManagerSel(event) {
    console.log(event.currentTarget.dataset.sel)
    this.setData({
      ManagerNames: [''],
      ManagerTitle: event.currentTarget.dataset.sel
    });
  },
  skill(e) {
    if (e.detail.value == '') {

    } else {
      console.log(e.detail.value)
      let list = this.data.skill
      list.push(e.detail.value)
      this.setData({
        skill: list
      })
    }
  },
  skillDel(e) {
    console.log(e.currentTarget.dataset.id)
    let list = this.data.skill
    list.splice(e.currentTarget.dataset.id, 1)
    this.setData({
      skill: list
    })
  },
  changeSwiper(e) {
    this.setData({
      current: e.currentTarget.dataset.id
    })
  },
  changeSwiper1(event) {
    this.setData({
      current: event.detail.current
    })
  },
  chooseVideo() {
    let that = this
    wx.chooseVideo({
      success(res) {
        console.log(res)
        that.setData({
          videoPath: res.tempFilePath,
          videoNmae: 'Selected'
        })
      }
    })
  },
  ChangeSwitch(res) {
    let name = res.currentTarget.dataset.name
    this.setData({
      [name]: res.detail
    });
  },
  formSubmit(event) {
    let self = this
    let form = {
      Title: event.detail.value.Title,
      Description: event.detail.value.Description,
      skill: self.data.skill
    }
    console.log(form)
    wx.navigateTo({
      url: `./Preview/Preview?data=${JSON.stringify(form)}`,
    })
  }
})