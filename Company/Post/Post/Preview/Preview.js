// Advisor/Course/Post/Preview/Preview.js
Page({

  /**
   * 页面的初始数据
   */
  data: {
    controls: false,
    van_icon: 0
  },
  onLoad(options){
    console.log(JSON.parse(options.data))
    this.setData(JSON.parse(options.data))
  },
  videoStop(){
    let that = this
    that.setData({
      van_icon: 0,
      controls: false
    })
  },
  videoPlay(){
    let that = this
    let videoplay = wx.createVideoContext('video')
    videoplay.play()
    that.setData({
      van_icon: 1,
      controls: true
    })
  },
  navBack(){
    wx.navigateBack({
      delta: 0
    })
  }
})