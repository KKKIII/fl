// Company/Post/Post.js
Page({

  /**
   * 页面的初始数据
   */
  data: {
    current: 0
  },
  navHome(){
    wx.navigateTo({
      url: '../Home/Home',
    })
  },
  navTabbar(e){
    console.log(e.currentTarget.dataset.url)
    wx.navigateTo({
      url: `../${e.currentTarget.dataset.url}/${e.currentTarget.dataset.url}`,
    })
  },
  changeTabs(e){
    this.setData({
      current: e.currentTarget.dataset.id
    })
  },
  changeCurrent(event){
    this.setData({
      current: event.detail.current
    })
  },
  onPullDownRefresh: function () {
    setTimeout(() => {
      wx.hideNavigationBarLoading() //隐藏标题栏显示加载状态
      wx.stopPullDownRefresh() //结束刷新
    }, 2000); //设置执行时间
  },
  navPost(){
    wx.navigateTo({
      url: './Post/Post',
    })
  }
})