// pages/Account/Profile/SaveJobs.js
Page({

  /**
   * 页面的初始数据
   */
  data: {
    jobs:['','','','']
  },
  navBack(){
    wx.navigateBack({
      delta: 0
    })
  },
  clearAll(){
    this.setData({jobs: ''})
  }
})