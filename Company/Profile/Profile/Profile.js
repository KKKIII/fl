// pages/Account/Profile/Profile.js
Page({

  /**
   * 页面的初始数据
   */
  data: {
    imgSrc: '',
    textarea: `Recent Graduate. Attaining a degree with Merit in 
    3D Computer Animation. Specialising in technical 
    animation, character modelling and texturing.
    Highly skilled in Maya, Zbrush, Substance Painter, 
    Marvelous Designer, Nuke and After Effects. 
    Alongside intermediate experience with Houdini.
    Currently working for Universal Studios in 
    Singapore as a Technical Animator, with a focus on 
    shows and attractions.`,
    Resume_fileName: '',
    Resume_videoName: '',
    Resume_file: [],
    Resume_video: [],
    Experience1:{
      "info": '',
      "name": '',
      "time": ''
    },
    Experience2:{
      "info": '',
      "name": '',
      "time": ''
    }
  },
  navBack(){
    wx.navigateBack({
      delta: 0
    })
  },
  chooseFile(){
    var that = this
    console.log('1')
    wx.chooseMessageFile({
      count: 1,
      type: "file",
      success(res){
        console.log(res.tempFiles[0])
        that.setData({
          Resume_file: res.tempFiles[0].path,
          Resume_fileName: res.tempFiles[0].name
        })
      }
    })
  },
  chooseVideo(){
    var that = this
    wx.chooseMessageFile({
      count: 1,
      type: "video",
      success(res){
        console.log(res.tempFiles[0].path)
        that.setData({
          Resume_video: res.tempFiles[0].path,
          Resume_videoName: res.tempFiles[0].name
        })
      }
    })
  },
  //工作经验
  ExpChooseImg1(){
    wx.chooseImage({
      count: 1,
      success(res){
        console.log(res)
      }
    })
  },
  exp1Info(e){
    let name = 'Experience1.info'
    this.setData({[name]: e.detail.value})
  },
  exp1Name(e){
    let name = 'Experience1.name'
    this.setData({[name]: e.detail.value})
  },
  exp1Time(e){
    let name = 'Experience1.time'
    this.setData({[name]: e.detail.value})
  },
  exp2Info(e){
    let name = 'Experience2.info'
    this.setData({[name]: e.detail.value})
  },
  exp2Name(e){
    let name = 'Experience2.name'
    this.setData({[name]: e.detail.value})
  },
  exp2Time(e){
    let name = 'Experience2.time'
    this.setData({[name]: e.detail.value})
  },
  modify(){
    let that = this
    wx.chooseImage({
      count: 1,
      success(res){
        console.log(res.tempFilePaths[0])
        that.setData({imgSrc: res.tempFilePaths[0]})
      }
    })
  }
})