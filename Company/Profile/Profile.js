// Company/Profile/Profile.js


Page({

  /**
   * 页面的初始数据
   */
  data: {
    email: ''
  },
  navFirst(){
    wx.navigateTo({
      url: '../../pages/Start/Start',
    })
  },
  navHome(){
    wx.navigateTo({
      url: '../Home/Home',
    })
  },
  onLoad(){
    console.log(getApp().globalData)
    this.setData(getApp().globalData)
  },
  navTabbar(e){
    console.log(e.currentTarget.dataset.url)
    wx.navigateTo({
      url: `../${e.currentTarget.dataset.url}/${e.currentTarget.dataset.url}`,
    })
  },
  navProfile(){
    wx.navigateTo({
      url: './Profile/Profile'
    })
  },
  navSettings(){
    wx.navigateTo({
      url: './Profile/Settings'
    })
  },
  navSaveJobs(){
    wx.navigateTo({
      url: './Profile/SaveJobs'
    })
  },
  onShareAppMessage(){
    return{
      imageUrl: '../../pages/static/FL.png',
      title: 'Future Leaders',
      path: '../Start/Start'
    }
  },
  share(){
    wx.showShareMenu({
      menus: [],
    })
  }
})